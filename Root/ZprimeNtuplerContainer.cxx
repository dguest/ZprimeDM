#include <ZprimeDM/ZprimeNtuplerContainer.h>

ZprimeNtuplerContainer::ZprimeNtuplerContainer()
{ }

ZprimeNtuplerContainer::ZprimeNtuplerContainer(ZprimeNtuplerContainer::Type type, const std::string& containerName, const std::string& branchName, const std::string& detailStr, const std::string& detailStrSyst)
  : m_type(type), m_containerName(containerName), m_branchName(branchName), m_detailStr(detailStr), m_detailStrSyst(detailStrSyst)
{ }
