#include <ZprimeDM/FatJetHists.h>

using namespace ZprimeDM;

FatJetHists :: FatJetHists (const std::string& name, const std::string& detailStr, const std::string& prefix)
  : HistogramManager(name, detailStr), m_debug(false), m_infoSwitch(detailStr), m_prefix(prefix)
{ }

FatJetHists :: ~FatJetHists () 
{ }

StatusCode FatJetHists::initialize()
{

  if(m_infoSwitch.m_kinematic)
    {
      h_pt   = book(m_name, "pt"    , m_prefix+" fat jet p_{T} [GeV]", 100, 0, 500);
      h_pt_m = book(m_name, "pt_m"  , m_prefix+" fat jet p_{T} [GeV]", 100, 0, 1000);
      h_pt_l = book(m_name, "pt_l"  , m_prefix+" fat jet p_{T} [GeV]", 100, 0, 5000);

      h_eta  = book(m_name, "eta"   , m_prefix+" fat jet #eta", 60, -3          , 3);
      h_phi  = book(m_name, "phi"   , m_prefix+" fat jet #phi", 50, -TMath::Pi(), TMath::Pi());

      h_m    = book(m_name, "m"     , m_prefix+" fat jet mass [GeV]" , 100, 0, 500);
      h_m_m  = book(m_name, "m_m"   , m_prefix+" fat jet mass [GeV]" , 100, 0, 1000);
      h_m_l  = book(m_name, "m_l"   , m_prefix+" fat jet mass [GeV]" , 100, 0, 5000);

      h_rho  = book(m_name, "rho"   , m_prefix+" fat jet #rho"       , 100, -10, 0);
    }

  if(m_infoSwitch.m_area)
    {
      h_GhostArea         = book(m_name, "GhostArea"         ,m_prefix+" fat jet GhostArea"         , 100, 0, 500);
      h_ActiveArea        = book(m_name, "ActiveArea"        ,m_prefix+" fat jet ActiveArea"        , 100, 0, 500);
      h_VoronoiArea       = book(m_name, "VoronoiArea"       ,m_prefix+" fat jet VoronoiArea"       , 100, 0, 500);
      h_ActiveArea4vec_pt = book(m_name, "ActiveArea4vec_pt" ,m_prefix+" fat jet ActiveArea4vec_pt" , 100, 0, 500);
      h_ActiveArea4vec_eta= book(m_name, "ActiveArea4vec_eta",m_prefix+" fat jet ActiveArea4vec_eta", 100, 0, 500);
      h_ActiveArea4vec_phi= book(m_name, "ActiveArea4vec_phi",m_prefix+" fat jet ActiveArea4vec_phi", 100, 0, 500);
      h_ActiveArea4vec_m  = book(m_name, "ActiveArea4vec_m"  ,m_prefix+" fat jet ActiveArea4vec_m"  , 100, 0, 500);
    }

  if(m_infoSwitch.m_substructure)
    {
      h_Split12        = book(m_name, "Split12"        ,m_prefix+" fat jet Split12"        , 100, 0, 200);
      h_Split23        = book(m_name, "Split23"        ,m_prefix+" fat jet Split23"        , 100, 0, 100);
      h_Split34        = book(m_name, "Split34"        ,m_prefix+" fat jet Split34"        , 100, 0,  50);
      h_Tau1_wta       = book(m_name, "Tau1_wta"       ,m_prefix+" fat jet Tau1_wta"       , 100, 0,   1);
      h_Tau2_wta       = book(m_name, "Tau2_wta"       ,m_prefix+" fat jet Tau2_wta"       , 100, 0, 0.5);
      h_Tau3_wta       = book(m_name, "Tau3_wta"       ,m_prefix+" fat jet Tau3_wta"       , 100, 0, 0.25);
      h_Tau21_wta      = book(m_name, "Tau21_wta"      ,m_prefix+" fat jet Tau21_wta"      ,  20, 0,   1);
      h_Tau32_wta      = book(m_name, "Tau32_wta"      ,m_prefix+" fat jet Tau32_wta"      ,  20, 0,   1);
      h_ECF1           = book(m_name, "ECF1"           ,m_prefix+" fat jet ECF1"           , 100, 0,1500);
      h_ECF2           = book(m_name, "ECF2"           ,m_prefix+" fat jet ECF2"           , 100, 0, 100e6);
      h_ECF3           = book(m_name, "ECF3"           ,m_prefix+" fat jet ECF3"           , 100, 0,2000e9);
      h_C2             = book(m_name, "C2"             ,m_prefix+" fat jet C2"             , 100, 0,   0.5);
      h_D2             = book(m_name, "D2"             ,m_prefix+" fat jet D2"             , 100, 0,   5);
      h_NTrimSubjets   = book(m_name, "NTrimSubjets"   ,m_prefix+" fat jet NTrimSubjets"   ,  10, -0.5, 9.5);
      h_MyNClusters    = book(m_name, "MyNClusters"    ,m_prefix+" fat jet MyNClusters"    ,  10, -0.5, 9.5);
      h_GhostTrackCount= book(m_name, "GhostTrackCount",m_prefix+" fat jet GhostTrackCount", 100, -0.5,99.5);
    }

  if(m_infoSwitch.m_constituent)
    {
      h_numConstituents= book(m_name, "numConstituents",m_prefix+" fat jet numConstituents", 100, -0.5,99.5);
    }

  if(m_infoSwitch.m_bosonCount)
    {
      h_GhostTQuarksFinalCount= book(m_name, "GhostTQuarksFinalCount",m_prefix+" fat jet GhostTQuarksFinalCount", 10, -0.5, 9.5);
      h_GhostWBosonsCount     = book(m_name, "GhostWBosonsCount"     ,m_prefix+" fat jet GhostWBosonsCount"     , 10, -0.5, 9.5);
      h_GhostZBosonsCount     = book(m_name, "GhostZBosonsCount"     ,m_prefix+" fat jet GhostZBosonsCount"     , 10, -0.5, 9.5);
      h_GhostHBosonsCount     = book(m_name, "GhostHBosonsCount"     ,m_prefix+" fat jet GhostHBosonsCount"     , 10, -0.5, 9.5);
    }

  if(m_infoSwitch.m_VTags)
    {
      h_Wtag_medium= book(m_name, "Wtag_medium",m_prefix+" fat jet Wtag_medium", 100, 0, 500);
      h_Ztag_medium= book(m_name, "Ztag_medium",m_prefix+" fat jet Ztag_medium", 100, 0, 500);

      h_Wtag_tight = book(m_name, "Wtag_tight" ,m_prefix+" fat jet Wtag_tight" , 100, 0, 500);
      h_Ztag_tight = book(m_name, "Ztag_tight" ,m_prefix+" fat jet Ztag_tight" , 100, 0, 500);
    }

  if(m_infoSwitch.m_trackJets)
    {
      h_nTrkJets  = book(m_name, "nTrkJets" ,m_prefix+" fat jet N_{trk jet}"  , 10, -0.5, 9.5);
      h_nTrkBJets = book(m_name, "nTrkBJets",m_prefix+" fat jet N_{trk b-jet}", 10, -0.5, 9.5);
    }

  return StatusCode::SUCCESS;
}

StatusCode FatJetHists::execute(const xAH::FatJet* fatjet, float eventWeight)
{
  ANA_CHECK(HistogramManager::execute());

  if(m_infoSwitch.m_kinematic)
    {
      h_pt   ->Fill(fatjet->p4.Pt() ,eventWeight);
      h_pt_m ->Fill(fatjet->p4.Pt() ,eventWeight);
      h_pt_l ->Fill(fatjet->p4.Pt() ,eventWeight);

      h_eta  ->Fill(fatjet->p4.Eta(),eventWeight);
      h_phi  ->Fill(fatjet->p4.Phi(),eventWeight);

      h_m    ->Fill(fatjet->p4.M() ,eventWeight);
      h_m_m  ->Fill(fatjet->p4.M() ,eventWeight);
      h_m_l  ->Fill(fatjet->p4.M() ,eventWeight);

      h_rho  ->Fill(log(pow(fatjet->p4.M(),2)/pow(fatjet->p4.Pt(),2)), eventWeight);
    }
  
  if(m_infoSwitch.m_area)
    {
      h_GhostArea         ->Fill(fatjet->GhostArea         , eventWeight);
      h_ActiveArea        ->Fill(fatjet->ActiveArea        , eventWeight);
      h_VoronoiArea       ->Fill(fatjet->VoronoiArea       , eventWeight);
      h_ActiveArea4vec_pt ->Fill(fatjet->ActiveArea4vec_pt , eventWeight);
      h_ActiveArea4vec_eta->Fill(fatjet->ActiveArea4vec_eta, eventWeight);
      h_ActiveArea4vec_phi->Fill(fatjet->ActiveArea4vec_phi, eventWeight);
      h_ActiveArea4vec_m  ->Fill(fatjet->ActiveArea4vec_m  , eventWeight);
    }

  if(m_infoSwitch.m_substructure)
    {
      h_Split12        ->Fill(fatjet->Split12        , eventWeight);
      h_Split23        ->Fill(fatjet->Split23        , eventWeight);
      h_Split34        ->Fill(fatjet->Split34        , eventWeight);
      h_Tau1_wta       ->Fill(fatjet->tau1_wta       , eventWeight);
      h_Tau2_wta       ->Fill(fatjet->tau2_wta       , eventWeight);
      h_Tau3_wta       ->Fill(fatjet->tau3_wta       , eventWeight);
      h_Tau21_wta      ->Fill(fatjet->tau21_wta      , eventWeight);
      h_Tau32_wta      ->Fill(fatjet->tau32_wta      , eventWeight);
      h_ECF1           ->Fill(fatjet->ECF1           , eventWeight);
      h_ECF2           ->Fill(fatjet->ECF2           , eventWeight);
      h_ECF3           ->Fill(fatjet->ECF3           , eventWeight);
      h_C2             ->Fill(fatjet->C2             , eventWeight);
      h_D2             ->Fill(fatjet->D2             , eventWeight);
      h_NTrimSubjets   ->Fill(fatjet->NTrimSubjets   , eventWeight);
      h_MyNClusters    ->Fill(fatjet->NClusters      , eventWeight);
      h_GhostTrackCount->Fill(fatjet->nTracks        , eventWeight);
    }

  if(m_infoSwitch.m_constituent)
    {
      h_numConstituents->Fill(fatjet->numConstituents, eventWeight);
    }

  if(m_infoSwitch.m_bosonCount)
    {
      h_GhostTQuarksFinalCount->Fill(fatjet->nTQuarks, eventWeight);
      h_GhostWBosonsCount     ->Fill(fatjet->nWBosons, eventWeight);
      h_GhostZBosonsCount     ->Fill(fatjet->nZBosons, eventWeight);
      h_GhostHBosonsCount     ->Fill(fatjet->nHBosons, eventWeight);
    }

  if(m_infoSwitch.m_VTags)
    {
      h_Wtag_medium->Fill(fatjet->Wtag_medium, eventWeight);
      h_Ztag_medium->Fill(fatjet->Ztag_medium, eventWeight);

      h_Wtag_tight ->Fill(fatjet->Wtag_tight , eventWeight);
      h_Ztag_tight ->Fill(fatjet->Ztag_tight , eventWeight);
    }

  if(m_infoSwitch.m_trackJets)
    {
      uint nTrkJets =0;
      uint nTrkBJets=0;
      for(const auto& trkJet : fatjet->trkJets)
	{
	  nTrkJets++;
	  if(trkJet.MV2c20_isFix77) nTrkBJets++;
	}
      h_nTrkJets ->Fill(nTrkJets , eventWeight);
      h_nTrkBJets->Fill(nTrkBJets, eventWeight);
    }

  return StatusCode::SUCCESS;
}

StatusCode FatJetHists::execute(const ZprimeDM::FatJet* fatjet, float eventWeight)
{
  ANA_CHECK(HistogramManager::execute());

  if(m_infoSwitch.m_kinematic)
    {
      h_pt   ->Fill(fatjet->p4.Pt() ,eventWeight);
      h_pt_m ->Fill(fatjet->p4.Pt() ,eventWeight);
      h_pt_l ->Fill(fatjet->p4.Pt() ,eventWeight);

      h_eta  ->Fill(fatjet->p4.Eta(),eventWeight);
      h_phi  ->Fill(fatjet->p4.Phi(),eventWeight);

      h_m    ->Fill(fatjet->p4.M() ,eventWeight);
      h_m_m  ->Fill(fatjet->p4.M() ,eventWeight);
      h_m_l  ->Fill(fatjet->p4.M() ,eventWeight);

      h_rho  ->Fill(log(pow(fatjet->p4.M(),2)/pow(fatjet->p4.Pt(),2)), eventWeight);
    }
  
  if(m_infoSwitch.m_area)
    {
      h_GhostArea         ->Fill(fatjet->GhostArea         , eventWeight);
      h_ActiveArea        ->Fill(fatjet->ActiveArea        , eventWeight);
      h_VoronoiArea       ->Fill(fatjet->VoronoiArea       , eventWeight);
      h_ActiveArea4vec_pt ->Fill(fatjet->ActiveArea4vec_pt , eventWeight);
      h_ActiveArea4vec_eta->Fill(fatjet->ActiveArea4vec_eta, eventWeight);
      h_ActiveArea4vec_phi->Fill(fatjet->ActiveArea4vec_phi, eventWeight);
      h_ActiveArea4vec_m  ->Fill(fatjet->ActiveArea4vec_m  , eventWeight);
    }

  if(m_infoSwitch.m_substructure)
    {
      h_Split12        ->Fill(fatjet->Split12        , eventWeight);
      h_Split23        ->Fill(fatjet->Split23        , eventWeight);
      h_Split34        ->Fill(fatjet->Split34        , eventWeight);
      h_Tau1_wta       ->Fill(fatjet->Tau1_wta       , eventWeight);
      h_Tau2_wta       ->Fill(fatjet->Tau2_wta       , eventWeight);
      h_Tau3_wta       ->Fill(fatjet->Tau3_wta       , eventWeight);
      h_Tau21_wta      ->Fill(fatjet->Tau21_wta      , eventWeight);
      h_Tau32_wta      ->Fill(fatjet->Tau32_wta      , eventWeight);
      h_ECF1           ->Fill(fatjet->ECF1           , eventWeight);
      h_ECF2           ->Fill(fatjet->ECF2           , eventWeight);
      h_ECF3           ->Fill(fatjet->ECF3           , eventWeight);
      h_C2             ->Fill(fatjet->C2             , eventWeight);
      h_D2             ->Fill(fatjet->D2             , eventWeight);
      h_NTrimSubjets   ->Fill(fatjet->NTrimSubjets   , eventWeight);
      h_MyNClusters    ->Fill(fatjet->MyNClusters    , eventWeight);
      h_GhostTrackCount->Fill(fatjet->GhostTrackCount, eventWeight);
    }

  if(m_infoSwitch.m_constituent)
    {
      h_numConstituents->Fill(fatjet->numConstituents, eventWeight);
    }

  if(m_infoSwitch.m_bosonCount)
    {
      h_GhostTQuarksFinalCount->Fill(fatjet->GhostTQuarksFinalCount, eventWeight);
      h_GhostWBosonsCount     ->Fill(fatjet->GhostWBosonsCount     , eventWeight);
      h_GhostZBosonsCount     ->Fill(fatjet->GhostZBosonsCount     , eventWeight);
      h_GhostHBosonsCount     ->Fill(fatjet->GhostHBosonsCount     , eventWeight);
    }

  if(m_infoSwitch.m_VTags)
    {
      h_Wtag_medium->Fill(fatjet->Wtag_medium, eventWeight);
      h_Ztag_medium->Fill(fatjet->Ztag_medium, eventWeight);

      h_Wtag_tight ->Fill(fatjet->Wtag_tight , eventWeight);
      h_Ztag_tight ->Fill(fatjet->Ztag_tight , eventWeight);
    }

  if(m_infoSwitch.m_trackJets)
    {
      uint nTrkJets =0;
      uint nTrkBJets=0;
      for(const auto& trkJet : fatjet->trkJets)
	{
	  nTrkJets++;
	  if(trkJet.isFix77) nTrkBJets++;
	}
      h_nTrkJets ->Fill(nTrkJets , eventWeight);
      h_nTrkBJets->Fill(nTrkBJets, eventWeight);
    }

  return StatusCode::SUCCESS;
}
