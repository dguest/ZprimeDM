#include <ZprimeDM/GammaJetHists.h>


GammaJetHists :: GammaJetHists (const std::string& name, const std::string& detailStr, const std::string& jetDetailStr, const std::string& photonDetailStr)
  : EventHists(name, detailStr),
    m_reso(0),
    m_jetDetailStr(jetDetailStr), h_nJet(0), m_jet0(0), m_jet1(0),
    m_photonDetailStr(photonDetailStr), h_nPhoton(0), m_photon0(0)
{ }

GammaJetHists :: ~GammaJetHists () 
{ }

void GammaJetHists::record(EL::Worker* wk)
{
  EventHists::record(wk);

  m_reso        ->record(wk);

  if(!m_jetDetailStr.empty())
    {
      m_jet0    ->record(wk);
      m_jet1    ->record(wk);
    }

  if(!m_photonDetailStr.empty())
    {
      m_photon0 ->record(wk);
    }
}

StatusCode GammaJetHists::initialize()
{
  if(m_debug) std::cout << "GammaJetHists::initialize()" << std::endl;

  ANA_CHECK(EventHists::initialize());

  m_reso        =new ResonanceHists(m_name+"reso_", "");
  ANA_CHECK(m_reso->initialize());

  if(!m_jetDetailStr.empty())
    {
      h_nJet     = book(m_name, "nJets",     "N_{jets}",      10,     -0.5,     9.5 );

      m_jet0=new ZprimeDM::JetHists(m_name+"jet0_" , m_jetDetailStr, "leading");
      ANA_CHECK(m_jet0->initialize());

      m_jet1=new ZprimeDM::JetHists(m_name+"jet1_" , m_jetDetailStr, "subleading");
      ANA_CHECK(m_jet1->initialize());
    }

  if(!m_photonDetailStr.empty())
    {
      h_nPhoton  = book(m_name, "nPhotons",  "N_{#gamma}",    10,     -0.5,     9.5 );

      m_photon0 =new ZprimeDM::PhotonHists(m_name+"photon0_", m_photonDetailStr, "leading");
      ANA_CHECK(m_photon0->initialize());
    }

  return StatusCode::SUCCESS;
}

StatusCode GammaJetHists::execute(const DijetISREvent& event, const xAH::Photon* photon0, const xAH::Jet* jet0, float eventWeight)
{
  if(m_debug) std::cout << "GammaJetHists::execute()" << std::endl;
  ANA_CHECK(EventHists::execute(event, eventWeight));

  ANA_CHECK(m_reso     ->execute(photon0, jet0     , eventWeight));

  if(!m_jetDetailStr.empty())
    {
      h_nJet   ->Fill(event.jets()   , eventWeight);

      if(event.jets()>0)    ANA_CHECK(m_jet0   ->execute(event.jetcont(0)   , eventWeight));
      if(event.jets()>1)    ANA_CHECK(m_jet1   ->execute(event.jetcont(1)   , eventWeight));
    }

  if(!m_photonDetailStr.empty())
    {
      h_nPhoton->Fill(event.photons(), eventWeight);

      if(event.photons()>0) ANA_CHECK(m_photon0->execute(event.photoncont(0), eventWeight));
    }

  return StatusCode::SUCCESS;
}

StatusCode GammaJetHists::finalize()
{
  if(m_debug) std::cout << "GammaJetHists::finalize()" << std::endl;
  
  ANA_CHECK(EventHists::finalize());

  ANA_CHECK(m_reso     ->finalize());
  delete m_reso;

  if(!m_jetDetailStr.empty())
    {
      ANA_CHECK(m_jet0   ->finalize());
      ANA_CHECK(m_jet1   ->finalize());

      delete m_jet0;
      delete m_jet1;
    }

  if(!m_photonDetailStr.empty())
    {
      ANA_CHECK(m_photon0->finalize());

      delete m_photon0;
    }

  return StatusCode::SUCCESS;
}
