#include <ZprimeDM/EventHists.h>

EventHists :: EventHists (const std::string& name, const std::string& detailStr)
  : HistogramManager(name, detailStr), m_debug(false)
{ }

EventHists :: ~EventHists () 
{ }

StatusCode EventHists::initialize()
{
  //
  // Event Level
  //
  h_NPV      = book(m_name, "NPV",       "NPV",             50,     -0.5,    49   );
  h_mu_ave   = book(m_name, "mu_ave",    "#mu_{ave}",       50,     -0.5,    49   );
  h_mu_act   = book(m_name, "mu_act",    "#mu_{act}",       50,     -0.5,    49   );

  h_weight   = book(m_name, "weight",    "MC Event Weight", 100,     -100,   100   );

  return StatusCode::SUCCESS;
}

StatusCode EventHists::execute(const DijetISREvent& event, float eventWeight)
{
  ANA_CHECK(HistogramManager::execute());

  h_NPV    ->Fill(event.m_NPV                           , eventWeight);
  h_mu_ave ->Fill(event.m_averageInteractionsPerCrossing, eventWeight);
  h_mu_act ->Fill(event.m_actualInteractionsPerCrossing , eventWeight);

  h_weight ->Fill(event.m_weight/event.m_weight_xs      , eventWeight);

  return StatusCode::SUCCESS;
}
