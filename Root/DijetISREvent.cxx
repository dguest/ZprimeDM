#include "ZprimeDM/DijetISREvent.h"

#include <iostream>

DijetISREvent* DijetISREvent::m_event=0;

DijetISREvent* DijetISREvent::global()
{
  if(DijetISREvent::m_event==0)
    DijetISREvent::m_event=new DijetISREvent();
  return m_event;
}

DijetISREvent::DijetISREvent()
  : m_doTruthOnly(false),
    m_jets_container(0), m_photons_container(0), m_fatjets_container(0), m_truth_container(0), m_trigJets_container(0),
    m_triggerInfoSwitch(0)
{
  setTriggerDetail("");

  m_passedTriggers  =new std::vector<std::string>();
  m_triggerPrescales=new std::vector<float>();

  m_jets     =new TClonesArray("ZprimeDM::Jet");
  m_photons  =new TClonesArray("ZprimeDM::Photon");
  m_fatjets  =new TClonesArray("ZprimeDM::FatJet");
  m_trigjets =new TClonesArray("ZprimeDM::Jet");
  m_truths   =new TClonesArray("ZprimeDM::TruthParticle");

  m_trphotons=new TClonesArray("ZprimeDM::TruthParticle");
}

DijetISREvent::~DijetISREvent()
{
  delete m_triggerInfoSwitch;

  delete m_passedTriggers;
  delete m_triggerPrescales;

  if(m_jets_container)     delete m_jets_container;
  if(m_photons_container)  delete m_photons_container;
  if(m_fatjets_container)  delete m_fatjets_container;
  if(m_truth_container)    delete m_truth_container;
  if(m_trigJets_container) delete m_trigJets_container;

  delete m_jets;
  delete m_photons;
  delete m_fatjets;
  delete m_trigjets;
  delete m_truths;

  delete m_trphotons;
}

void DijetISREvent::setTree(TTree *tree)
{
  m_tree=tree;

  //
  // Connect branches
  tree->SetBranchStatus ("*", 0);

  // Event info
  tree->SetBranchStatus  ("runNumber",    1);
  tree->SetBranchAddress ("runNumber",    &m_runNumber);

  tree->SetBranchStatus  ("eventNumber",    1);
  tree->SetBranchAddress ("eventNumber",    &m_eventNumber);

  if(!m_doTruthOnly)
    {
      tree->SetBranchStatus  ("lumiBlock",    1);
      tree->SetBranchAddress ("lumiBlock",    &m_lumiBlock);

      tree->SetBranchStatus  ("NPV",    1);
      tree->SetBranchAddress ("NPV",    &m_NPV);

      tree->SetBranchStatus  ("actualInteractionsPerCrossing",    1);
      tree->SetBranchAddress ("actualInteractionsPerCrossing",    &m_actualInteractionsPerCrossing);

      tree->SetBranchStatus  ("averageInteractionsPerCrossing",    1);
      tree->SetBranchAddress ("averageInteractionsPerCrossing",    &m_averageInteractionsPerCrossing);

      tree->SetBranchStatus  ("weight_pileup", 1);
      tree->SetBranchAddress ("weight_pileup", &m_weight_pileup);
    }

  if(m_mc)
    {
      tree->SetBranchStatus  ("mcEventNumber", 1);
      tree->SetBranchAddress ("mcEventNumber", &m_mcEventNumber);

      tree->SetBranchStatus  ("mcChannelNumber", 1);
      tree->SetBranchAddress ("mcChannelNumber", &m_mcChannelNumber);

      tree->SetBranchStatus  ("mcEventWeight", 1);
      tree->SetBranchAddress ("mcEventWeight", &m_mcEventWeight);
    }

  if(m_triggerInfoSwitch->m_passTriggers)
    {
      tree->SetBranchStatus  ("passedTriggers",   1);
      tree->SetBranchAddress ("passedTriggers",   &m_passedTriggers);

      tree->SetBranchStatus  ("triggerPrescales", 1);
      tree->SetBranchAddress ("triggerPrescales", &m_triggerPrescales);
    }

  // weights
  tree->SetBranchStatus  ("weight", 1);
  tree->SetBranchAddress ("weight", &m_weight);

  tree->SetBranchStatus  ("weight_xs", 1);
  tree->SetBranchAddress ("weight_xs", &m_weight_xs);

  // particles
  if(tree->GetBranch("jet"    ))
    {
      tree->SetBranchStatus ("jet"      , 1);
      tree->SetBranchStatus ("jet.*"    , 1);
      tree->SetBranchAddress("jet"      , &m_jets);
    }
  if(tree->GetBranch("ph"     ))
    {
      tree->SetBranchStatus ("ph"       , 1);
      tree->SetBranchStatus ("ph.*"     , 1);
      tree->SetBranchAddress("ph"       , (!m_doTruthOnly)?(&m_photons):(&m_trphotons));
    }
  if(tree->GetBranch("fatjet" ))
    {
      tree->SetBranchStatus ("fatjet"   , 1);
      tree->SetBranchStatus ("fatjet.*" , 1);
      tree->SetBranchAddress("fatjet"   , &m_fatjets);
    }
  if(tree->GetBranch("trigjet"))
    {
      tree->SetBranchStatus ("trigjet"  , 1);
      tree->SetBranchStatus ("trigjet.*", 1);
      tree->SetBranchAddress("trigjet"  , &m_trigjets);
    }
  if(tree->GetBranch("truth"  ))
    {
      tree->SetBranchStatus ("truth"  , 1);
      tree->SetBranchAddress("truth"  , &m_truths);
    }

  // containers (old style particles)
  if(m_jets_container)     m_jets_container    ->setTree(tree);
  if(m_photons_container)  m_photons_container ->setTree(tree);
  if(m_fatjets_container)  m_fatjets_container ->setTree(tree);
  if(m_truth_container)    m_truth_container   ->setTree(tree);
  if(m_trigJets_container) m_trigJets_container->setTree(tree);

  // custom
  if(tree->GetBranch("Zprime_pt")) 
    {
      tree->SetBranchStatus ("Zprime_pt", 1);
      tree->SetBranchAddress("Zprime_pt", &m_Zprime_pt);
    }

  if(tree->GetBranch("Zprime_eta")) 
    {
      tree->SetBranchStatus ("Zprime_eta", 1);
      tree->SetBranchAddress("Zprime_eta", &m_Zprime_eta);
    }

  if(tree->GetBranch("Zprime_phi")) 
    {
      tree->SetBranchStatus ("Zprime_phi", 1);
      tree->SetBranchAddress("Zprime_phi", &m_Zprime_phi);
    }

  if(tree->GetBranch("Zprime_m")) 
    {
      tree->SetBranchStatus ("Zprime_m", 1);
      tree->SetBranchAddress("Zprime_m", &m_Zprime_m);
    }

}

void DijetISREvent::updateEntry()
{
  if(m_doTruthOnly)
    {
      m_photons->Clear();
      for(int i=0; i<m_trphotons->GetEntriesFast(); i++)
	{
	  const ZprimeDM::TruthParticle *tr = static_cast<const ZprimeDM::TruthParticle*>(m_trphotons->At(i));
	        ZprimeDM::Photon        *ph = static_cast<      ZprimeDM::Photon       *>(m_photons->ConstructedAt(i));

	  ph->p4=tr->p4;
	}

    }

  if(m_jets_container)     m_jets_container    ->updateEntry();
  if(m_photons_container)  m_photons_container ->updateEntry();
  if(m_fatjets_container)  m_fatjets_container ->updateEntry();
  if(m_truth_container)    m_truth_container   ->updateEntry();
  if(m_trigJets_container) m_trigJets_container->updateEntry();

  m_Zprime.SetPtEtaPhiM(m_Zprime_pt,m_Zprime_eta,m_Zprime_phi,m_Zprime_m);
}

void DijetISREvent::setTriggerDetail(const std::string &detailStr)
{
  if(m_triggerInfoSwitch) delete m_triggerInfoSwitch;
  m_triggerInfoSwitch=new HelperClasses::TriggerInfoSwitch(detailStr);
}

void DijetISREvent::initializeJets(const std::string& name, const std::string& detailStr)
{
  m_jets_container    =new xAH::JetContainer   (name, detailStr, 1e3, m_mc);
}

void DijetISREvent::initializePhotons(const std::string& name, const std::string& detailStr)
{
  m_photons_container =new xAH::PhotonContainer(name, detailStr, 1e3, m_mc);
}

void DijetISREvent::initializeFatJets(const std::string& name, const std::string& detailStr)
{
  m_fatjets_container =new xAH::FatJetContainer(name, detailStr, "", 1e3, m_mc);
}

void DijetISREvent::initializeTruth(const std::string& name, const std::string& detailStr)
{
  m_truth_container   =new xAH::TruthContainer(name, detailStr);
}

void DijetISREvent::initializeTrigJets(const std::string& name, const std::string& detailStr)
{
  m_trigJets_container=new xAH::JetContainer   (name, detailStr, 1e3, m_mc);
}



bool DijetISREvent::haveJetsCont() const
{ return m_jets_container!=0; }

uint DijetISREvent::jets() const
{ return (!m_jets_container)?m_jets->GetEntries():m_jets_container->size(); }

const ZprimeDM::Jet* DijetISREvent::jet(uint idx) const
{ return static_cast<const ZprimeDM::Jet*>(m_jets->At(idx));  }

const xAH::Jet* DijetISREvent::jetcont(uint idx) const
{ return &m_jets_container->at(idx);  }


bool DijetISREvent::havePhotonsCont() const
{ return m_photons_container!=0; }

uint DijetISREvent::photons() const
{ return (!m_photons_container)?m_photons->GetEntries():m_photons_container->size(); }

const xAH::Photon* DijetISREvent::photoncont(uint idx) const
{ return &m_photons_container->at(idx);  }

const ZprimeDM::Photon* DijetISREvent::photon(uint idx) const
{ return static_cast<const ZprimeDM::Photon*>(m_photons->At(idx)); }


bool DijetISREvent::haveFatJetsCont() const
{ return m_fatjets_container!=0; }

uint DijetISREvent::fatjets() const
{ return (!m_fatjets_container)?m_fatjets->GetEntries():m_fatjets_container->size(); }

const ZprimeDM::FatJet* DijetISREvent::fatjet(uint idx) const
{ return static_cast<const ZprimeDM::FatJet*>(m_fatjets->At(idx));  }

const xAH::FatJet* DijetISREvent::fatjetcont(uint idx) const
{ return &m_fatjets_container->at(idx);  }


bool DijetISREvent::haveTrigJetsCont() const
{ return m_trigJets_container!=0; }

uint DijetISREvent::trigjets() const
{ return (!m_trigJets_container)?m_trigjets->GetEntries():m_trigJets_container->size(); }

const ZprimeDM::Jet* DijetISREvent::trigjet(uint idx) const
{ return static_cast<const ZprimeDM::Jet*>(m_trigjets->At(idx));  }

const xAH::Jet* DijetISREvent::trigjetcont(uint idx) const
{ return &m_trigJets_container->at(idx);  }


bool DijetISREvent::haveTruthsCont() const
{ return m_truth_container!=0; }

uint DijetISREvent::truths() const
{ return (!m_truth_container)?m_truths->GetEntries():m_truth_container->size(); }

const ZprimeDM::TruthParticle* DijetISREvent::truth(uint idx) const
{ return static_cast<const ZprimeDM::TruthParticle*>(m_truths->At(idx));  }

const xAH::TruthPart* DijetISREvent::truthcont(uint idx) const
{ return &m_truth_container->at(idx);  }
