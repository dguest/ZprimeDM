#include <ZprimeDM/ZprimeNtupler.h>

#include <EventLoop/Job.h>
#include <EventLoop/Worker.h>
#include <EventLoop/OutputStream.h>
#include <AthContainers/ConstDataVector.h>

#include <SampleHandler/MetaFields.h>

#include <xAODTracking/VertexContainer.h>
#include <xAODJet/JetContainer.h>
#include <xAODTrigger/JetRoIContainer.h>
#include <xAODEventInfo/EventInfo.h>

#include <xAODAnaHelpers/HelperFunctions.h>

// this is needed to distribute the algorithm to the workers
ClassImp(ZprimeNtupler)

ZprimeNtupler :: ZprimeNtupler (const std::string& className) :
ZprimeAlgorithm(className),
  m_cleanPileup(false),m_treeStream("tree")
{
  ANA_MSG_INFO("ZprimeNtupler::ZprimeNtupler()");
  m_inputAlgo                = "";
  m_isMC                     = false;
  m_truthLevelOnly           = false;
  m_eventDetailStr           = "truth pileup";
  m_trigDetailStr            = "";
}

EL::StatusCode ZprimeNtupler :: setupJob (EL::Job& job)
{
  job.useXAOD();
  xAOD::Init( "ZprimeNtupler" ).ignore(); // call before opening first file

  EL::OutputStream outForTree( m_treeStream );
  job.outputAdd (outForTree);
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode ZprimeNtupler :: initialize ()
{
  ANA_CHECK(ZprimeAlgorithm::initialize());

  m_eventCounter = -1;

  //
  // configuration
  const xAOD::EventInfo* eventInfo(nullptr);
  ANA_CHECK(HelperFunctions::retrieve(eventInfo, "EventInfo", m_event, m_store));
  m_isMC = eventInfo->eventType( xAOD::EventInfo::IS_SIMULATION );

  // Pythia dijet JZ*W samples
  m_cleanPileup = ( m_isMC && 361020<=eventInfo->mcChannelNumber() && eventInfo->mcChannelNumber()<=361032 );
  if(m_cleanPileup)
    m_cf_cleanPileup=init_cutflow("CleanPileup");

  Info("initialize()", "Succesfully initialized! \n");
  return EL::StatusCode::SUCCESS;
}

void ZprimeNtupler::AddTree( const std::string& name ) {

  std::string treeName("outTree");
  // naming convention
  treeName += name; // add systematic
  TTree * outTree = new TTree(treeName.c_str(),treeName.c_str());
  if( !outTree ) {
    Error("AddTree()","Failed to get output tree!");
    // FIXME!! kill here
  }
  TFile* treeFile = wk()->getOutputFile( m_treeStream );
  outTree->SetDirectory( treeFile );

  //
  // Create the output tree
  //
  ZprimeMiniTree* miniTree = new ZprimeMiniTree(m_event, outTree, treeFile);
  if( m_truthLevelOnly ) {
    miniTree->AddEvent("truth");
  }else{
    miniTree->AddEvent( m_eventDetailStr );
    miniTree->AddTrigger( m_trigDetailStr );
  }
  
  for(auto container : m_containers)
    {
      switch(container.m_type)
	{
	case ZprimeNtuplerContainer::JET:
	  miniTree->AddJets      ((name.empty())?container.m_detailStr:container.m_detailStrSyst, container.m_branchName);
	  break;
	case ZprimeNtuplerContainer::PHOTON:
	  miniTree->AddPhotons   ((name.empty())?container.m_detailStr:container.m_detailStrSyst, container.m_branchName);
	  break;
	case ZprimeNtuplerContainer::TRUTH:
	  miniTree->AddTruthParts(container.m_branchName, (name.empty())?container.m_detailStr:container.m_detailStrSyst);
	  break;
	case ZprimeNtuplerContainer::FATJET:
	  miniTree->AddFatJets   ((name.empty())?container.m_detailStr:container.m_detailStrSyst, container.m_branchName);
	  break;	  
	}
    }
  m_myTrees[name] = miniTree;
  // see Worker.cxx line 134: the following function call takes ownership of the tree
  // from the treeFile; no output is written. so don't do that!
  // wk()->addOutput( outTree );
}


EL::StatusCode ZprimeNtupler :: execute ()
{
  // Here you do everything that needs to be done on every single
  // event, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.
  ANA_MSG_DEBUG("ZprimeNtupler::execute()");
  ++m_eventCounter;

  //
  // Pythia dijet sample cleaning
  if(m_cleanPileup)
    {
      const xAOD::JetContainer* recoJets=0;
      ANA_CHECK(HelperFunctions::retrieve(recoJets,  "AntiKt4EMTopoJets", m_event, m_store));

      const xAOD::JetContainer* truthJets=0;
      ANA_CHECK(HelperFunctions::retrieve(truthJets, "AntiKt4TruthJets",  m_event, m_store));

      if(recoJets->size() > 1) {
	float pTAvg = ( recoJets->at(0)->pt() + recoJets->at(1)->pt() ) /2.0;
	if( truthJets->size() == 0 || (pTAvg / truthJets->at(0)->pt() > 1.4) ) {
	  wk()->skipEvent();
	  return EL::StatusCode::SUCCESS;
	}
      }

      cutflow(m_cf_cleanPileup);
    }


  //
  // Retrieve Input Containers
  //
  ANA_MSG_DEBUG("Get Containers");
  const xAOD::EventInfo* eventInfo = 0;
  ANA_CHECK(HelperFunctions::retrieve(eventInfo, "EventInfo", m_event, m_store));


  SG::AuxElement::ConstAccessor<float> NPVAccessor("NPV");
  const xAOD::VertexContainer* vertices = 0;
  if(!m_truthLevelOnly) {
    ANA_CHECK(HelperFunctions::retrieve(vertices, "PrimaryVertices", m_event, m_store));
    if(!NPVAccessor.isAvailable( *eventInfo )) { // NPV might already be available
      // number of PVs with 2 or more tracks
      //eventInfo->auxdecor< int >( "NPV" ) = HelperFunctions::countPrimaryVertices(vertices, 2);
      // TMP for JetUncertainties uses the same variable
      eventInfo->auxdecor< float >( "NPV" ) = HelperFunctions::countPrimaryVertices(vertices, 2);
    }
  }

  //
  // Event weights
  //
  if(m_isMC && (eventInfo->mcChannelNumber()!=345342 && eventInfo->mcChannelNumber()!=341566))
    m_mcEventWeight = eventInfo->mcEventWeight();
  else
    m_mcEventWeight = 1;

  double xs     =wk()->metaData()->castDouble(SH::MetaFields::crossSection    ,1);
  double eff    =wk()->metaData()->castDouble(SH::MetaFields::filterEfficiency,1);
  double kfactor=wk()->metaData()->castDouble(SH::MetaFields::kfactor         ,1);

  eventInfo->auxdecor< float >("weight_xs") = xs * eff * kfactor;
  eventInfo->auxdecor< float >("weight")    = m_mcEventWeight * xs * eff * kfactor;

  //
  // Truth info
  //
  if(m_truthLevelOnly)
    {
      ANA_MSG_DEBUG("Finding truth Zprime");

      float Zprime_pt =0;
      float Zprime_eta=0;
      float Zprime_phi=0;
      float Zprime_m  =0;

      const xAOD::TruthParticleContainer* truths;
      if(HelperFunctions::isAvailable<xAOD::TruthParticleContainer>("TruthParticles",m_event,m_store))
	{
	  ANA_CHECK(HelperFunctions::retrieve(truths, "TruthParticles", m_event, m_store));

	  for(auto truth : *truths)
	    {
	      if(truth->pdgId()==101)
		{
		  Zprime_pt =truth->pt();
		  if(Zprime_pt>0)
		    {
		      Zprime_eta=truth->eta();
		      Zprime_phi=truth->phi();
		    }
		  Zprime_m  =truth->m();
		  break;
		}
	    }
	}

      eventInfo->auxdecor< float >("Zprime_pt")  = Zprime_pt/1000;
      eventInfo->auxdecor< float >("Zprime_eta") = Zprime_eta;
      eventInfo->auxdecor< float >("Zprime_phi") = Zprime_phi;
      eventInfo->auxdecor< float >("Zprime_m")   = Zprime_m/1000;
    }

  //
  // Loop over Systematics
  //

  //
  // did any collection pass the cuts?
  //
  bool pass(false);

  //
  // if input comes from xAOD, or just running one collection, 
  // then get the one collection and be done with it
  //
  if( m_inputAlgo.empty() || m_truthLevelOnly ) {

    if(m_truthLevelOnly)
      {
  	pass = this->executeTruthAnalysis( eventInfo );
      }
    else
      {
  	pass = this->executeAnalysis( eventInfo, vertices );
      }

  }

  //
  // get the list of systematics to run over
  //
  else 
    { 

      //
      // get vector of string giving the names
      //
      std::vector<std::string>* systNames = 0;
      if ( m_store->contains< std::vector<std::string> >( m_inputAlgo ) ) 
      	{
      	  if(!m_store->retrieve( systNames, m_inputAlgo ).isSuccess()) 
      	    {
      	      ANA_MSG_INFO("Cannot find vector from " << m_inputAlgo);
      	      return StatusCode::FAILURE;
      	    }
      	}
    
      //
      // loop over systematics
      //
      bool saveContainerNames(false);
      std::vector< std::string >* vecOutContainerNames = 0;
      if(saveContainerNames) { vecOutContainerNames = new std::vector< std::string >; }

      //
      // should only count for the nominal
      //
      bool passOne(false);
      for( auto systName : *systNames ) 
      	{
      	  //
      	  // align with Dijet naming conventions
      	  //
      	  passOne = this->executeAnalysis( eventInfo, vertices, systName );

      	  //
      	  // save the string if passing the selection
      	  //
      	  if( saveContainerNames && passOne ) { vecOutContainerNames->push_back( systName ); }

      	  //
      	  // the final decision - if at least one passes keep going!
      	  //
      	  pass = pass || passOne;
 
       	}

      //
      // save list of systs that should be considered down stream
      //
      if( saveContainerNames ) 
      	{
      	  ANA_CHECK( m_store->record( vecOutContainerNames, m_name));
      	}
    }

  //
  // kill events if nothing passes
  //
  if(!pass) 
    wk()->skipEvent();

  return EL::StatusCode::SUCCESS;
  
}

bool ZprimeNtupler :: executeTruthAnalysis ( const xAOD::EventInfo* eventInfo,
					     const std::string& systName) {
  ANA_MSG_DEBUG("ZprimeNtupler::executeTruthAnalysis()");

  //
  //  fill the tree
  //
  ANA_MSG_DEBUG(" Filling Branches");
  if( m_myTrees.find( systName ) == m_myTrees.end() ) AddTree( systName );

  ANA_MSG_DEBUG(" Filling Event");
  if(eventInfo) m_myTrees[systName]->FillEvent( eventInfo, m_event );

  ANA_MSG_DEBUG(" Filling Particles");

  for(auto containerInfo : m_containers)
    {
      ANA_MSG_DEBUG(" Filling " << containerInfo.m_containerName);
      switch(containerInfo.m_type)
	{
	case ZprimeNtuplerContainer::JET:
	  {
	    const xAOD::JetContainer* jets = 0;
	    ANA_CHECK(HelperFunctions::retrieve(jets,    containerInfo.m_containerName+systName, m_event, m_store));
	    m_myTrees[systName]->FillJets( jets, -1, containerInfo.m_branchName );
	    break;
	  }

	case ZprimeNtuplerContainer::PHOTON:
	  {
	    const xAOD::PhotonContainer* photons = 0;
	    ANA_CHECK(HelperFunctions::retrieve(photons, containerInfo.m_containerName+systName, m_event, m_store));
	    m_myTrees[systName]->FillPhotons( photons, containerInfo.m_branchName );
	    break;
	  }

	case ZprimeNtuplerContainer::TRUTH:
	  {
	    const xAOD::TruthParticleContainer* truths = 0;
	    ANA_CHECK(HelperFunctions::retrieve(truths, containerInfo.m_containerName+systName, m_event, m_store));
	    m_myTrees[systName]->FillTruth( containerInfo.m_branchName, truths );
	    break;
	  }

	case ZprimeNtuplerContainer::FATJET:
	  {
	    const xAOD::JetContainer* fatjets = 0;
	    ANA_CHECK(HelperFunctions::retrieve(fatjets,    containerInfo.m_containerName+systName, m_event, m_store));
	    m_myTrees[systName]->FillFatJets( fatjets, containerInfo.m_branchName );
	    break;
	  }
	}
    }

  ANA_MSG_DEBUG(" Fill Tree");
  m_myTrees[systName]->Fill();

  return true;
}

bool ZprimeNtupler :: executeAnalysis ( const xAOD::EventInfo* eventInfo,
					const xAOD::VertexContainer* vertices,
					const std::string& systName)
{
  ANA_MSG_DEBUG("ZprimeNtupler::executeAnalysis()");

  //
  //  fill the tree
  //
  ANA_MSG_DEBUG(" Filling Branches");
  if( m_myTrees.find( systName ) == m_myTrees.end() ) AddTree( systName );

  ANA_MSG_DEBUG(" Filling Event");
  if(eventInfo) m_myTrees[systName]->FillEvent( eventInfo, m_event );

  ANA_MSG_DEBUG(" Filling Trigger");
  if(eventInfo) m_myTrees[systName]->FillTrigger(eventInfo);

  ANA_MSG_DEBUG(" Filling Particles");

  for(auto containerInfo : m_containers)
    {
      if(m_msgLevel < 3) std::cout << " Filling " << containerInfo.m_containerName << std::endl;
      switch(containerInfo.m_type)
	{
	case ZprimeNtuplerContainer::JET:
	  {
	    const xAOD::JetContainer* jets = 0;
	    ANA_CHECK(HelperFunctions::retrieve(jets,    containerInfo.m_containerName+systName, m_event, m_store));
	    m_myTrees[systName]->FillJets( jets, HelperFunctions::getPrimaryVertexLocation( vertices ), containerInfo.m_branchName );
	    break;
	  }

	case ZprimeNtuplerContainer::PHOTON:
	  {
	    const xAOD::PhotonContainer* photons = 0;
	    ANA_CHECK(HelperFunctions::retrieve(photons, containerInfo.m_containerName+systName, m_event, m_store));
	    m_myTrees[systName]->FillPhotons( photons, containerInfo.m_branchName );
	    break;
	  }

	case ZprimeNtuplerContainer::TRUTH:
	  {
	    const xAOD::TruthParticleContainer* truths = 0;
	    ANA_CHECK(HelperFunctions::retrieve(truths, containerInfo.m_containerName+systName, m_event, m_store));
	    m_myTrees[systName]->FillTruth( containerInfo.m_branchName, truths );
	    break;
	  }

	case ZprimeNtuplerContainer::FATJET:
	  {
	    const xAOD::JetContainer* fatjets = 0;
	    ANA_CHECK(HelperFunctions::retrieve(fatjets, containerInfo.m_containerName+systName, m_event, m_store));
	    m_myTrees[systName]->FillFatJets( fatjets, containerInfo.m_branchName );
	    break;
	  }

	}
    }

  ANA_MSG_DEBUG(" Fill Tree");
  m_myTrees[systName]->Fill();

  return true;
}
EL::StatusCode ZprimeNtupler :: histFinalize ()
{ 
  ANA_CHECK(ZprimeAlgorithm::histFinalize());
  
  write_cutflow(m_treeStream, "Test");
      
  // Write out the Metadata
  //
  TFile *fileMD = wk()->getOutputFile("metadata");
  TH1D* histEventCount = (TH1D*)fileMD->Get("MetaData_EventCount");

  TFile *treeFile = wk()->getOutputFile( m_treeStream );
  TH1F* thisHistEventCount = (TH1F*) histEventCount->Clone();
  std::string thisName = thisHistEventCount->GetName();
  thisHistEventCount->SetName( (thisName+"_Test").c_str() );
  thisHistEventCount->SetDirectory( treeFile );

  return EL::StatusCode::SUCCESS;
}
