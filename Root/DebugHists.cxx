#include <ZprimeDM/DebugHists.h>

DebugHists :: DebugHists (const std::string& name, const std::string& detailStr)
  : HistogramManager(name, detailStr)
{}

DebugHists :: ~DebugHists () 
{}

StatusCode DebugHists::initialize()
{
  //
  //  2D plots
  //
  h_reso1_ptvsreso0_pt = book(m_name, "h_reso1_ptvsreso0_pt","Leading Resonance Jet p_{T} [GeV]",    100, 0, 1000, "Subleading Resonance Jet p_{T} [GeV]", 100, 0, 1000);
  h_isr_ptvsreso0_pt   = book(m_name, "h_isr_ptvsreso0_pt"  ,"Leading Resonance Jet p_{T} [GeV]",    100, 0, 1000, "ISR p_{T} [GeV]", 100, 0, 1000);
  h_isr_ptvsreso1_pt   = book(m_name, "h_isr_ptvsreso1_pt"  ,"Subleading Resonance Jet p_{T} [GeV]", 100, 0, 1000, "ISR p_{T} [GeV]", 100, 0, 1000);

  h_dPhijjvsreso0_pt = book(m_name, "h_dPhijjvsreso0_pt","Leading Resonance Jet p_{T} [GeV]",    100, 0, 1000, "|#Delta#phi_{jj}|", 100, 0, TMath::Pi());
  h_dPhijjvsreso1_pt = book(m_name, "h_dPhijjvsreso1_pt","Subleading Resonance Jet p_{T} [GeV]", 100, 0, 1000, "|#Delta#phi_{jj}|", 100, 0, TMath::Pi());
  h_dPhijjvsisr_pt   = book(m_name, "h_dPhijjvsisr_pt"  ,"ISR p_{T} [GeV]",                      100, 0, 1000, "|#Delta#phi_{jj}|", 100, 0, TMath::Pi());
  h_dPhijjvsptjj     = book(m_name, "h_dPhijjvsptjj"    ,"p_{T,jj} [GeV]",                       100, 0, 1000, "|#Delta#phi_{jj}|", 100, 0, TMath::Pi());

  h_dPhijjvsasymISR1   = book(m_name, "h_dPhijjvsasymISR1","(|p_{T}^{ISR}|-|p_{T}^{1}|)/(|p_{T}^{ISR}|+|p_{T}^{1}|)", 120, 0, 1, "|#Delta#phi_{jj}|", 100, 0, TMath::Pi());
  h_dPhijjvsasymISR2   = book(m_name, "h_dPhijjvsasymISR2","(|p_{T}^{ISR}|-|p_{T}^{2}|)/(|p_{T}^{ISR}|+|p_{T}^{2}|)", 120, 0, 1, "|#Delta#phi_{jj}|", 100, 0, TMath::Pi());

  h_dPhijjvsdRISRclosej = book(m_name, "h_dPhijjvsdRISRclosej","#DeltaR_{ISR,close-j}", 100, 0, 5, "|#Delta#phi_{jj}|", 100, 0, TMath::Pi());

  h_asymjjvsreso0_pt = book(m_name, "h_asymjjvsreso0_pt","Leading Resonance Jet p_{T} [GeV]",    100, 0, 1000, "(|p_{T}^{1}|-|p_{T}^{2}|)/(|p_{T}^{1}|+|p_{T}^{2}|)", 120, 0, 1);
  h_asymjjvsreso1_pt = book(m_name, "h_asymjjvsreso1_pt","Subleading Resonance Jet p_{T} [GeV]", 100, 0, 1000, "(|p_{T}^{1}|-|p_{T}^{2}|)/(|p_{T}^{1}|+|p_{T}^{2}|)", 120, 0, 1);
  h_asymjjvsisr_pt   = book(m_name, "h_asymjjvsisr_pt"  ,"ISR p_{T} [GeV]",                      100, 0, 1000, "(|p_{T}^{1}|-|p_{T}^{2}|)/(|p_{T}^{1}|+|p_{T}^{2}|)", 120, 0, 1);
  h_asymjjvsptjj     = book(m_name, "h_asymjjvsptjj"    ,"p_{T,jj} [GeV]",                       100, 0, 1000, "(|p_{T}^{1}|-|p_{T}^{2}|)/(|p_{T}^{1}|+|p_{T}^{2}|)", 120, 0, 1);

  h_asymjjvsdRISRclosej = book(m_name, "h_asymjjvsdRISRclosej","#DeltaR_{ISR,close-j}", 100, 0, 5, "(|p_{T}^{1}|-|p_{T}^{2}|)/(|p_{T}^{1}|+|p_{T}^{2}|)", 120, 0, 1);

  h_projasymjjvsreso0_pt = book(m_name, "h_projasymjjvsreso0_pt","Leading Resonance Jet p_{T} [GeV]",    100, 0, 1000, "(#vec{p}_{T}^{1}-#vec{p}_{T}^{2})#upoint#hat{p}_{T}^{jj}/p_{T}^{jj}", 50, 0, 5);
  h_projasymjjvsreso1_pt = book(m_name, "h_projasymjjvsreso1_pt","Subleading Resonance Jet p_{T} [GeV]", 100, 0, 1000, "(#vec{p}_{T}^{1}-#vec{p}_{T}^{2})#upoint#hat{p}_{T}^{jj}/p_{T}^{jj}", 50, 0, 5);
  h_projasymjjvsisr_pt   = book(m_name, "h_projasymjjvsisr_pt"  ,"ISR p_{T} [GeV]",                      100, 0, 1000, "(#vec{p}_{T}^{1}-#vec{p}_{T}^{2})#upoint#hat{p}_{T}^{jj}/p_{T}^{jj}", 50, 0, 5);
  h_projasymjjvsptjj     = book(m_name, "h_projasymjjvsptjj"    ,"p_{T,jj} [GeV]",                       100, 0, 1000, "(#vec{p}_{T}^{1}-#vec{p}_{T}^{2})#upoint#hat{p}_{T}^{jj}/p_{T}^{jj}", 50, 0, 5);

  h_projasymjjvsdRISRclosej = book(m_name, "h_projasymjjvsdRISRclosej","#DeltaR_{ISR,close-j}", 100, 0, 5, "(#vec{p}_{T}^{1}-#vec{p}_{T}^{2})#upoint#hat{p}_{T}^{jj}/p_{T}^{jj}", 50, 0, 5);

  h_mjjvsreso0_pt = book(m_name, "h_mjjvsreso0_pt","Leading Resonance Jet p_{T} [GeV]",    100, 0, 1000, "m_{jj} [GeV]", 100, 0, 1000);
  h_mjjvsreso1_pt = book(m_name, "h_mjjvsreso1_pt","Subleading Resonance Jet p_{T} [GeV]", 100, 0, 1000, "m_{jj} [GeV]", 100, 0, 1000);
  h_mjjvsisr_pt   = book(m_name, "h_mjjvsisr_pt"  ,"ISR p_{T} [GeV]",                      100, 0, 1000, "m_{jj} [GeV]", 100, 0, 1000);
  h_mjjvsptjj     = book(m_name, "h_mjjvsptjj"    ,"p_{T,jj} [GeV]",                       100, 0, 1000, "m_{jj} [GeV]", 100, 0, 1000);

  h_mjjvsreso0_eta= book(m_name, "h_mjjvsreso0_eta","Leading Resonance Jet #eta",    60, -3, 3, "m_{jj} [GeV]", 100, 0, 1000);
  h_mjjvsreso1_eta= book(m_name, "h_mjjvsreso1_eta","Subleading Resonance Jet #eta", 60, -3, 3, "m_{jj} [GeV]", 100, 0, 1000);
  h_mjjvsisr_eta  = book(m_name, "h_mjjvsisr_eta"  ,"ISR #eta",                      60, -3, 3, "m_{jj} [GeV]", 100, 0, 1000);

  h_mjjvsdPhijj     = book(m_name, "h_mjjvsdPhijj","|#Delta#phi_{jj}|", 100, 0, TMath::Pi(), "m_{jj} [GeV]", 100, 0, 1000);
  h_mjjvsdRjj       = book(m_name, "h_mjjvsdRjj","|#DeltaR_{jj}|"     , 100, 0,           5, "m_{jj} [GeV]", 100, 0, 1000);
  h_mjjvsasymjj     = book(m_name, "h_mjjvsasymjj","(|p_{T}^{1}|-|p_{T}^{2}|)/(|p_{T}^{1}|+|p_{T}^{2}|)"                    , 120, 0, 1, "m_{jj} [GeV]", 100, 0, 1000);
  h_mjjvsprojasymjj = book(m_name, "h_mjjvsprojasymjj","(#vec{p}_{T}^{1}-#vec{p}_{T}^{2})#upoint#hat{p}_{T}^{jj}/p_{T}^{jj}",  50, 0, 5, "m_{jj} [GeV]", 100, 0, 1000);

  h_mjjvsasymISR1   = book(m_name, "h_mjjvsasymISR1","(|p_{T}^{ISR}|-|p_{T}^{1}|)/(|p_{T}^{ISR}|+|p_{T}^{1}|)", 120, 0, 1, "m_{jj} [GeV]", 100, 0, 1000);
  h_mjjvsasymISR2   = book(m_name, "h_mjjvsasymISR2","(|p_{T}^{ISR}|-|p_{T}^{2}|)/(|p_{T}^{ISR}|+|p_{T}^{2}|)", 120, 0, 1, "m_{jj} [GeV]", 100, 0, 1000);

  h_mjjvsdRISRclosej = book(m_name, "h_mjjvsdRISRclosej","#DeltaR_{ISR,close-j}", 100, 0, 5, "m_{jj} [GeV]", 100, 0, 1000);

  h_dPhijjvsasymjj     = book(m_name, "h_dPhijjvsasymjj"    ,"(|p_{T}^{1}|-|p_{T}^{2}|)/(|p_{T}^{1}|+|p_{T}^{2}|)",                 120, 0, 1, "|#Delta#phi_{jj}|", 100, 0, TMath::Pi());
  h_dPhijjvsprojasymjj = book(m_name, "h_dPhijjvsprojasymjj","(#vec{p}_{T}^{1}-#vec{p}_{T}^{2})#upoint#hat{p}_{T}^{jj}/p_{T}^{jj}",  50, 0, 5, "|#Delta#phi_{jj}|", 100, 0, TMath::Pi());

  return StatusCode::SUCCESS;
}

StatusCode DebugHists::execute(const xAH::Jet* reso0, const xAH::Jet* reso1, const xAH::Particle* ISR, float eventWeight)
{
  TLorentzVector p4_Zprime=reso0->p4+reso1->p4;
  double mjj=p4_Zprime.M();

  h_reso1_ptvsreso0_pt->Fill(reso0->p4.Pt(), reso1->p4.Pt(), eventWeight);
  if(ISR) h_isr_ptvsreso0_pt  ->Fill(reso0->p4.Pt(), ISR->p4.Pt(),   eventWeight);
  if(ISR) h_isr_ptvsreso1_pt  ->Fill(reso1->p4.Pt(), ISR->p4.Pt(),   eventWeight);

  h_mjjvsreso0_pt->Fill(reso0->p4.Pt(), mjj, eventWeight);
  h_mjjvsreso1_pt->Fill(reso1->p4.Pt(), mjj, eventWeight);
  if(ISR) h_mjjvsisr_pt->Fill(ISR  ->p4.Pt(), mjj, eventWeight);
  h_mjjvsptjj    ->Fill(p4_Zprime.Pt(), mjj, eventWeight);

  h_mjjvsreso0_eta->Fill(reso0->p4.Eta(), mjj, eventWeight);
  h_mjjvsreso1_eta->Fill(reso1->p4.Eta(), mjj, eventWeight);
  if(ISR) h_mjjvsisr_eta->Fill(ISR  ->p4.Eta(), mjj, eventWeight);

  double dphijj=reso0->p4.DeltaPhi(reso1->p4);
  h_mjjvsdPhijj->Fill(dphijj, mjj, eventWeight);

  double dRjj=reso0->p4.DeltaR(reso1->p4);
  h_mjjvsdRjj->Fill(dRjj, mjj, eventWeight);

  double asymjj = (reso0->p4.Pt()-reso1->p4.Pt())/(reso0->p4.Pt()+reso1->p4.Pt());
  h_mjjvsasymjj->Fill(asymjj, mjj, eventWeight);

  TVector3 p2_Zprime=p4_Zprime.Vect();
  p2_Zprime.SetZ(0);
  p2_Zprime.SetMag(1.);
  double projasymjj = ((reso0->p4-reso1->p4).Vect().Dot(p2_Zprime))/p4_Zprime.Pt();
  h_mjjvsprojasymjj->Fill(projasymjj, mjj, eventWeight);

  h_dPhijjvsasymjj    ->Fill(asymjj,     dphijj, eventWeight);
  h_dPhijjvsprojasymjj->Fill(projasymjj, dphijj, eventWeight);

  h_dPhijjvsreso0_pt->Fill(reso0->p4.Pt(), dphijj, eventWeight);
  h_dPhijjvsreso1_pt->Fill(reso1->p4.Pt(), dphijj, eventWeight);
  if(ISR) h_dPhijjvsisr_pt->Fill(ISR->p4.Pt(), dphijj, eventWeight);
  h_dPhijjvsptjj    ->Fill(p4_Zprime.Pt(), dphijj, eventWeight);

  h_asymjjvsreso0_pt->Fill(reso0->p4.Pt(), asymjj, eventWeight);
  h_asymjjvsreso1_pt->Fill(reso1->p4.Pt(), asymjj, eventWeight);
  if(ISR) h_asymjjvsisr_pt->Fill(ISR->p4.Pt(), asymjj, eventWeight);
  h_asymjjvsptjj    ->Fill(p4_Zprime.Pt(), asymjj, eventWeight);

  h_projasymjjvsreso0_pt->Fill(reso0->p4.Pt(), projasymjj, eventWeight);
  h_projasymjjvsreso1_pt->Fill(reso1->p4.Pt(), projasymjj, eventWeight);
  if(ISR) h_projasymjjvsisr_pt->Fill(ISR->p4.Pt(), projasymjj, eventWeight);
  h_projasymjjvsptjj    ->Fill(p4_Zprime.Pt(), projasymjj, eventWeight);

  if(ISR)
    {
      double asymISR1 = (ISR->p4.Pt()-reso0->p4.Pt())/(ISR->p4.Pt()+reso0->p4.Pt());
      double asymISR2 = (ISR->p4.Pt()-reso1->p4.Pt())/(ISR->p4.Pt()+reso1->p4.Pt());

      h_mjjvsasymISR1->Fill(asymISR1, mjj, eventWeight);
      h_mjjvsasymISR2->Fill(asymISR2, mjj, eventWeight);

      h_dPhijjvsasymISR1->Fill(asymISR1, dphijj, eventWeight);
      h_dPhijjvsasymISR2->Fill(asymISR2, dphijj, eventWeight);

      //
      //  close/far jet to ISR
      //
      TLorentzVector closej = reso0->p4;
      TLorentzVector farj   = reso1->p4;
      if(ISR->p4.DeltaR(reso1->p4) < ISR->p4.DeltaR(reso0->p4)){
	closej = reso1->p4;
	farj   = reso0->p4;
      }

      double dRISRclosej=ISR->p4.DeltaR(closej);

      h_dPhijjvsdRISRclosej    ->Fill(dRISRclosej, dphijj,     eventWeight);
      h_asymjjvsdRISRclosej    ->Fill(dRISRclosej, asymjj,     eventWeight);
      h_projasymjjvsdRISRclosej->Fill(dRISRclosej, projasymjj, eventWeight);
      h_mjjvsdRISRclosej       ->Fill(dRISRclosej, mjj,        eventWeight);
    }




  return StatusCode::SUCCESS;
}
