#include <ZprimeDM/ZprimeGammaJetJetHistsAlgo.h>

#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>

#include <AsgTools/MessageCheck.h>

#include <xAODAnaHelpers/HelperFunctions.h>
#include <xAODAnaHelpers/HelperClasses.h>

#include <algorithm>

// this is needed to distribute the algorithm to the workers
ClassImp(ZprimeGammaJetJetHistsAlgo)

ZprimeGammaJetJetHistsAlgo :: ZprimeGammaJetJetHistsAlgo ()
: ZprimeHistsBaseAlgo(),
  hSysPhotonIDup(nullptr),
  hSysPhotonIDdw(nullptr)
{
  m_minPhotonPt       = 10;
  m_doBarrelEndCap    = 3;
  m_phTrigMatch       = false;
  m_emuTrigJet        = false;
  m_emuTrigJetOverlap = false;
}

void ZprimeGammaJetJetHistsAlgo :: initISRCutflow ()
{
  m_cf_njets       = m_cutflow->addCut("NJets");
  m_cf_nphotons    = m_cutflow->addCut("NPhotons");
  m_cf_photon      = m_cutflow->addCut("Photon");
  if(m_phTrigMatch)       m_cf_phTrigMatch       = m_cutflow->addCut("PhTrigMatch");
  if(m_emuTrigJet)        m_cf_emuTrigJet        = m_cutflow->addCut("EmuTrigJet");
  if(m_emuTrigJetOverlap) m_cf_emuTrigJetOverlap = m_cutflow->addCut("EmuTrigJetOverlap");

  if(m_doBarrelEndCap==1)      m_cf_barrelendcap=m_cutflow->addCut("Barrel");
  else if(m_doBarrelEndCap==2) m_cf_barrelendcap=m_cutflow->addCut("EndCap");
}

bool ZprimeGammaJetJetHistsAlgo :: doISRCutflow ()
{
  //
  // dijet+gamma
  uint njets    = m_event->jets   ();
  uint nphotons = m_event->photons();

  if(njets < 2)
    {
      ANA_MSG_DEBUG(" Fail NJets with " << njets);
      return false;
    }
  m_cutflow->execute(m_cf_njets,m_eventWeight);

  if(nphotons < 1)
    {
      ANA_MSG_DEBUG(" Fail NPhotons with " << nphotons);
      return false;
    }
  m_cutflow->execute(m_cf_nphotons,m_eventWeight);

  const xAH::Photon *photon0=m_event->photoncont(0);

  //
  // photon
  if(photon0->p4.Pt() < m_minPhotonPt)
    {
      ANA_MSG_DEBUG(" Fail PhotonPt0 ");
      return false;
    }
  if(m_mc) m_eventWeight*=photon0->TightEffSF;
  m_cutflow->execute(m_cf_photon,m_eventWeight);

  //
  // trigger matching
  if(m_phTrigMatch)
    {
      bool passTrigMatch=true;
      for(const std::string& trigger : m_triggers)
	passTrigMatch &= (std::find(photon0->trigMatched.begin(), photon0->trigMatched.end(), trigger ) != photon0->trigMatched.end());

      if(!passTrigMatch)
	{
	  ANA_MSG_DEBUG(" Fail Trigger Matching ");
	  return false;
	}

      m_cutflow->execute(m_cf_phTrigMatch, m_eventWeight);
    }

  if(m_emuTrigJet)
    {
      unsigned int nTrigJets=0;
      for(unsigned int idx=0; idx<m_event->trigjets(); idx++)
	if(m_event->trigjetcont(idx)->p4.Pt()>50) nTrigJets++;
      
      if(nTrigJets<3)
	{
	  ANA_MSG_DEBUG("Fail trigger jet emulation");
	  return false;
	}
      m_cutflow->execute(m_cf_emuTrigJet, m_eventWeight);
    }

  if(m_emuTrigJetOverlap)
    {
      bool passTrigJetOverlap=false;
      for(unsigned int idx=0; idx<std::min<uint>(3,m_event->trigjets()); idx++)
	if(photon0->p4.DeltaR(m_event->trigjetcont(idx)->p4)<0.2) { passTrigJetOverlap=true; break; }
      
      if(!passTrigJetOverlap)
	{
	  ANA_MSG_DEBUG("Fail trigger jet overlap");
	  return false;
	}
      m_cutflow->execute(m_cf_emuTrigJetOverlap, m_eventWeight);
    }


  //
  // Barrel vs endcap
 switch(m_doBarrelEndCap)
   {
   case 1:
     if(!(fabs(photon0->p4.Eta())<1.37))
       {
	 ANA_MSG_DEBUG(" Fail PhotonBarrel ");
	 return false;
       }
     m_cutflow->execute(m_cf_barrelendcap,m_eventWeight);
     break;
   case 2:
     if(!(1.52<fabs(photon0->p4.Eta()) && fabs(photon0->p4.Eta())<2.37))
       {
	 ANA_MSG_DEBUG(" Fail PhotonEndCap ");
	 return false;
       }
     m_cutflow->execute(m_cf_barrelendcap,m_eventWeight);
     break;
   default:
     break;
   }

  //
  // Setup objects
  //
  m_reso0=m_event->jetcont(0);
  m_reso1=m_event->jetcont(1);
  m_isr  =photon0;

  return true;
}

EL::StatusCode ZprimeGammaJetJetHistsAlgo :: histInitialize ()
{
  ANA_MSG_INFO("ZprimeGammaJetJetHistsAlgo::histInitialize()");

  //
  // Histograms
  ANA_CHECK(ZprimeHistsBaseAlgo::histInitialize())

  if(!m_doDetails) return EL::StatusCode::SUCCESS;

  if(m_mc)
    {
      hSysPhotonIDup=new DijetISRHists(m_name+"/sysPhotonID__1up/", "", m_jetDetailStr, m_photonDetailStr);
      ANA_CHECK(hSysPhotonIDup->initialize());
      hSysPhotonIDup->record(wk());

      hSysPhotonIDdw=new DijetISRHists(m_name+"/sysPhotonID__1down/", "", m_jetDetailStr, m_photonDetailStr);
      ANA_CHECK(hSysPhotonIDdw->initialize());
      hSysPhotonIDdw->record(wk());
    }

  if(m_event->haveTrigJetsCont())
    {
      hPhotonTrigger=new PhotonTriggerHists(m_name, "", "kinematic");
      ANA_CHECK(hPhotonTrigger->initialize());
      hPhotonTrigger->record(wk());
    }

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode ZprimeGammaJetJetHistsAlgo :: histFill (float eventWeight)
{
  ANA_CHECK(ZprimeHistsBaseAlgo::histFill(eventWeight))

  if(!m_doDetails) return EL::StatusCode::SUCCESS;

  const xAH::Photon *isrphoton=static_cast<const xAH::Photon*>(m_isr);

  if(m_mc)
    {
      hSysPhotonIDup->execute(*m_event, m_reso0, m_reso1, m_isr, eventWeight/isrphoton->TightEffSF*(isrphoton->TightEffSF+isrphoton->TightEffSF_Error));
      hSysPhotonIDdw->execute(*m_event, m_reso0, m_reso1, m_isr, eventWeight/isrphoton->TightEffSF*(isrphoton->TightEffSF+isrphoton->TightEffSF_Error));
    }

  if(m_event->haveTrigJetsCont())
    {
      const xAH::Jet *trigJet0=(m_event->trigjets()>0)?m_event->trigjetcont(0):0;
      const xAH::Jet *trigJet1=(m_event->trigjets()>1)?m_event->trigjetcont(1):0;
      const xAH::Jet *trigJet2=(m_event->trigjets()>2)?m_event->trigjetcont(2):0;
      hPhotonTrigger->execute(isrphoton, trigJet0, trigJet1, trigJet2, eventWeight);
    }

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode ZprimeGammaJetJetHistsAlgo :: histFinalize ()
{
  ANA_CHECK(ZprimeHistsBaseAlgo::histFinalize())
  
  if(!m_doDetails) return EL::StatusCode::SUCCESS;

  if(m_mc)
    {
      ANA_CHECK(hSysPhotonIDup->finalize());
      delete hSysPhotonIDup;

      ANA_CHECK(hSysPhotonIDdw->finalize());
      delete hSysPhotonIDdw;
    }

  return EL::StatusCode::SUCCESS;
}

