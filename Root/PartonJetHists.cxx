#include <ZprimeDM/PartonJetHists.h>

PartonJetHists :: PartonJetHists (const std::string& name) :
  HistogramManager(name, "")
{}

PartonJetHists :: ~PartonJetHists () 
{}

StatusCode PartonJetHists::initialize()
{
  // These plots are always made
  m_dPt           = book(m_name, "dPt",   "P_{T,jet}-P_{T,parton} [GeV]", 120, -100, 100);

  m_dEta          = book(m_name, "dEta",  "#Delta#eta_{jet,parton}", 120, 0          , 8);
  m_dPhi          = book(m_name, "dPhi",  "#Delta#phi_{jet,parton}", 120, -TMath::Pi(), TMath::Pi());
  m_dR            = book(m_name, "dR",    "#DeltaR_{jet,parton}"   , 120, -4         , 4);

  m_ptvspt        = book(m_name, "ptvspt","P_{T,jet} [GeV]", 200, 0, 1000,"P_{T,parton} [GeV]", 200, 0, 1000);

  return StatusCode::SUCCESS;
}

StatusCode PartonJetHists::execute( const xAOD::Jet *jet, const xAOD::TruthParticle *parton, float eventWeight)
{
  float dPt = (jet->pt()-parton->pt())/1e3;
  m_dPt->Fill(dPt, eventWeight);
  
  float dEta = fabs(jet->eta()-parton->eta());
  m_dEta->Fill(dEta, eventWeight);

  float dPhi = jet->p4().DeltaPhi(parton->p4());
  m_dPhi->Fill( dPhi  , eventWeight);

  float dR = jet->p4().DeltaR(parton->p4());
  m_dR  ->Fill(dR     , eventWeight);

  m_ptvspt->Fill(jet->pt()/1e3,parton->pt()/1e3,eventWeight);

  return StatusCode::SUCCESS;
}
