#include <EventLoop/Job.h>
#include <EventLoop/Worker.h>
#include <EventLoop/OutputStream.h>

#include <AsgTools/MessageCheck.h>

#include <ZprimeDM/ZprimeMGTrijetAlgo.h>
#include <xAODAnaHelpers/HelperFunctions.h>

#include "TFile.h"
#include "TKey.h"
#include "TLorentzVector.h"
#include "TSystem.h"

#include <utility>      
#include <iostream>
#include <fstream>

using namespace std;

// this is needed to distribute the algorithm to the workers
ClassImp(ZprimeMGTrijetAlgo)

ZprimeMGTrijetAlgo :: ZprimeMGTrijetAlgo () :
  m_truthDetailStr(""),
  m_jetPtCut(25.),
  m_leadJetPtCut(25.),
  m_ystarCut(-1),
  m_ystarJet0(0),
  m_ystarJet1(0)
{
  ANA_MSG_INFO("ZprimeMGTrijetAlgo::ZprimeMGTrijetAlgo()");
}

EL::StatusCode ZprimeMGTrijetAlgo :: histInitialize ()
{
  ANA_CHECK(xAH::Algorithm::algInitialize());
  ANA_MSG_INFO("ZprimeMGTrijetAlgo::histInitialize()");

  //
  // data model
  m_event=DijetISREvent::global();

  //
  // Cutflow
  m_cutflow=new CutflowHists(m_name, "");
  ANA_CHECK(m_cutflow->initialize());

  m_cf_jets =m_cutflow->addCut("jets");
  m_cf_jet0 =m_cutflow->addCut("jet0");
  if(m_ystarCut>0) m_cf_ystar=m_cutflow->addCut("ystar");

  m_cutflow->record(wk());

  //
  // Histograms
  const static std::string titles[3]={"leading","subleading","third"};
  for(uint jetIdx=0;jetIdx<3;jetIdx++)
    {
      h_jet_reso[jetIdx]=new ZprimeDM::TruthHists(m_name+"/jet"+std::to_string(jetIdx)+"_reso/", m_truthDetailStr, titles[jetIdx]+" jet");
      ANA_CHECK(h_jet_reso[jetIdx]->initialize());
      h_jet_reso[jetIdx]->record(wk());

      h_jet_isr[jetIdx] =new ZprimeDM::TruthHists(m_name+"/jet"+std::to_string(jetIdx)+"_isr/" , m_truthDetailStr, titles[jetIdx]+" jet");
      ANA_CHECK(h_jet_isr[jetIdx] ->initialize());
      h_jet_isr[jetIdx] ->record(wk());
    }

  h_hist2d =new ZprimeMGTrijet2DHists(m_name+"/hist2d/", "");
  ANA_CHECK(h_hist2d->initialize());
  h_hist2d ->record(wk());

  h_hist2d_m12 =new ZprimeMGTrijet2DHists(m_name+"/hist2d_m12/", "");
  ANA_CHECK(h_hist2d_m12->initialize());
  h_hist2d_m12 ->record(wk());

  h_hist2d_m13 =new ZprimeMGTrijet2DHists(m_name+"/hist2d_m13/", "");
  ANA_CHECK(h_hist2d_m13->initialize());
  h_hist2d_m13 ->record(wk());

  h_hist2d_m23 =new ZprimeMGTrijet2DHists(m_name+"/hist2d_m23/", "");
  ANA_CHECK(h_hist2d_m23->initialize());
  h_hist2d_m23 ->record(wk());

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode ZprimeMGTrijetAlgo :: execute ()
{
  ANA_MSG_DEBUG("ZprimeMGTrijetAlgo::initialize()");

  //
  // Cuts
  float eventWeight    =m_event->m_weight;

  //
  // Identify particles
  const xAH::TruthPart* jets[3] = {0,0,0};
  bool  jets_isReso[3];

  const xAH::TruthPart* jet_isr;
  const xAH::TruthPart* jet_reso0;
  const xAH::TruthPart* jet_reso1;

  uint lastJetIdx=0;
  for(uint truthIdx=0; truthIdx < m_event->truths(); truthIdx++)
    {
      const xAH::TruthPart *truth=m_event->truthcont(truthIdx);
      if(truth->status==1)
	{
	  jets[lastJetIdx]=truth;
	  lastJetIdx++;
	}
    }

  std::sort(std::begin(jets),std::end(jets),ZprimeHelperClasses::TruthSorter::sort);

  // Pick elements
  lastJetIdx=0;
  for(uint jetIdx=0;jetIdx<3;jetIdx++)
    {
      uint parentPdgId=jets[jetIdx]->parent_pdgId[0];
      jets_isReso[jetIdx]=(parentPdgId==101);
      if(parentPdgId==101)
	{
	  if(lastJetIdx==0)
	    {
	      jet_reso0=jets[jetIdx];
	      lastJetIdx++;
	    }
	  else
	    jet_reso1=jets[jetIdx];
	}
      else
	jet_isr=jets[jetIdx];
    }

  //
  // Need three jets  
  for(uint jetIdx=0;jetIdx<3;jetIdx++)
    {
      if(jets[jetIdx]->p4.Pt() < m_jetPtCut)
	{
	  ANA_MSG_DEBUG(" Fail jet (idx = " << jetIdx << ")");
	  return EL::StatusCode::SUCCESS;
	}
    }
  m_cutflow->execute(m_cf_jets,eventWeight);

  //
  // Need jet0
  if(jets[0]->p4.Pt() < m_leadJetPtCut)
    {
      ANA_MSG_DEBUG(" Fail lead jet");
      return EL::StatusCode::SUCCESS;
    }
  m_cutflow->execute(m_cf_jet0,eventWeight);

  //
  // Need ystar
  if(m_ystarCut>0)
    {
      double ystarjj =fabs(jets[m_ystarJet0]->p4.Rapidity()-jets[m_ystarJet1]->p4.Rapidity())/2.;
      if(ystarjj>m_ystarCut)
	{
	ANA_MSG_DEBUG(" Fail Ystar with " << ystarjj);
	  return EL::StatusCode::SUCCESS;
	}
      m_cutflow->execute(m_cf_ystar,eventWeight);
    }


  //
  // Histograms
  for(uint jetIdx=0;jetIdx<3;jetIdx++)
    {
      if(jets_isReso[jetIdx])
	h_jet_reso[jetIdx]->execute(jets[jetIdx], eventWeight);
      else
	h_jet_isr [jetIdx]->execute(jets[jetIdx], eventWeight);
    }

  h_hist2d->execute(jets, eventWeight);
  if(jets_isReso[0] && jets_isReso[1])
    h_hist2d_m12->execute(jets, eventWeight);
  else if(jets_isReso[0] && jets_isReso[2])
    h_hist2d_m13->execute(jets, eventWeight);
  else if(jets_isReso[1] && jets_isReso[2])
    h_hist2d_m23->execute(jets, eventWeight);

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode ZprimeMGTrijetAlgo :: histFinalize ()
{
  ANA_CHECK(m_cutflow->finalize());
  delete m_cutflow;

  for(uint jetIdx=0;jetIdx<3;jetIdx++)
    {
      ANA_CHECK(h_jet_reso[jetIdx]->finalize());
      delete h_jet_reso[jetIdx];

      ANA_CHECK(h_jet_isr [jetIdx]->finalize());
      delete h_jet_isr [jetIdx];
    }

  ANA_CHECK(h_hist2d->finalize());
  delete h_hist2d;

  ANA_CHECK(h_hist2d_m12->finalize());
  delete h_hist2d_m12;

  ANA_CHECK(h_hist2d_m13->finalize());
  delete h_hist2d_m13;

  ANA_CHECK(h_hist2d_m23->finalize());
  delete h_hist2d_m23;

  return EL::StatusCode::SUCCESS;
}
