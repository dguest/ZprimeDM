#include <ZprimeDM/ZprimeTruthHists.h>

ZprimeTruthHists :: ZprimeTruthHists (const std::string& name) :
  HistogramManager(name, "")
{}

ZprimeTruthHists :: ~ZprimeTruthHists () 
{}

StatusCode ZprimeTruthHists::initialize()
{
  // These plots are always made
  m_Zm            = book(m_name, "Zm"  ,  "Z' m [GeV]"     ,1100, 0           , 5500);

  //m_Zpt           = book(m_name, "Zpt" ,  "Z' P_{T} [GeV]" ,120 , 0           , 3000);
  //m_Zphi          = book(m_name, "Zphi",  "Z' #phi"        ,120 , -TMath::Pi(),TMath::Pi());
  //m_Zeta          = book(m_name, "Zeta",  "Z' #eta"        ,120 , -4          , 4);

  return StatusCode::SUCCESS;
}

StatusCode ZprimeTruthHists::execute( const xAOD::TruthParticleContainer* truthContainer, float eventWeight ) 
{
  //
  // Find Z'
  const xAOD::TruthParticle *Zp = 0;
  for(const auto& truth : *truthContainer)
    {
      if(truth->pdgId()==101)
	{
	  Zp = truth;
	  break;
	}
    }

  if(Zp==0)
    {
      Error("execute()","Zprime not found!");
      return StatusCode::FAILURE;
    }

  m_Zm->Fill(Zp->m()/1e3   , eventWeight);
  //m_Zpt->Fill(Zp->pt()/1e3 , eventWeight);
  //m_Zeta->Fill(Zp->eta()   , eventWeight);
  //m_Zphi->Fill(Zp->phi()   , eventWeight);

  return StatusCode::SUCCESS;
}
