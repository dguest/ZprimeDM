#include <EventLoop/Job.h>
#include <EventLoop/Worker.h>
#include "EventLoop/OutputStream.h"
#include "AthContainers/ConstDataVector.h"

#include "PathResolver/PathResolver.h"

#include "xAODTracking/VertexContainer.h"
#include <xAODJet/JetContainer.h>
#include "xAODEventInfo/EventInfo.h"
#include <ZprimeDM/ProcessTrigStudy.h>
#include <xAODAnaHelpers/HelperFunctions.h>

#include "TFile.h"
#include "TKey.h"
#include "TLorentzVector.h"
#include "TSystem.h"

#include <utility>      
#include <iostream>
#include <fstream>

using namespace std;

// this is needed to distribute the algorithm to the workers
ClassImp(ProcessTrigStudy)

ProcessTrigStudy :: ProcessTrigStudy () :
  m_applyGRL(true),
  m_GRLxml(""), // removing default to ensure correct file gets set
  m_debug(false),
  m_detailLevel(0),
  m_doData(true),
  m_doCleaning(false),
  m_lumi(0),
  m_sampleEvents(0),
  m_grl(nullptr),
  m_jet_pt(0),
  m_jet_eta(0),
  m_jet_phi(0),
  m_jet_E(0),
  m_jet_Timing(0),
  m_jet_GhostMuonSegmentCount(0),
  m_jet_clean_passLooseBad(0),
  m_passedTriggers(nullptr),
  m_gamma_pt(0),
  m_gamma_eta(0),
  m_gamma_phi(0),
  m_gamma_E(0),
  m_gamma_isIsolated_Cone40CaloOnly(0),
  m_gamma_isIsolated_Cone40        (0),
  m_gamma_isIsolated_Cone20        (0),
  m_gamma_IsLoose                  (0),
  m_gamma_IsMedium                 (0),
  m_gamma_IsTight                  (0),
  hIncl(nullptr),
  h_g35_loose(nullptr),
  h_g35_Tight(nullptr),
  h_g35_TightIso(nullptr),
  h_g45_loose(nullptr),
  h_g45_Tight(nullptr),
  h_g45_TightIso(nullptr),
  h_g55_loose(nullptr),
  h_g55_Tight(nullptr),
  h_g55_TightIso(nullptr),
  h_g65_loose(nullptr),
  h_g65_Tight(nullptr),
  h_g65_TightIso(nullptr),
  h_g75_loose(nullptr),
  h_g75_Tight(nullptr),
  h_g75_TightIso(nullptr)
{
  Info("ProcessTrigStudy()", "Calling constructor");
  m_applyGRL      = true;
  m_GRLxml   = "";  //https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/GoodRunListsForAnalysis
}


EL::StatusCode  ProcessTrigStudy :: configure ()
{


  hIncl                 = new basicHists("Incl"          ,       wk(), 2.8, m_detailLevel);

  h_g35_loose           = new basicHists("g35_loose"     ,       wk(), 2.8, m_detailLevel);
  h_g35_Tight           = new basicHists("g35_Tight"     ,       wk(), 2.8, m_detailLevel);
  h_g35_TightIso        = new basicHists("g35_TightIso"  ,       wk(), 2.8, m_detailLevel);

  h_g45_loose           = new basicHists("g45_loose"     ,       wk(), 2.8, m_detailLevel);
  h_g45_Tight           = new basicHists("g45_Tight"     ,       wk(), 2.8, m_detailLevel);
  h_g45_TightIso        = new basicHists("g45_TightIso"  ,       wk(), 2.8, m_detailLevel);

  h_g55_loose           = new basicHists("g55_loose"     ,       wk(), 2.8, m_detailLevel);
  h_g55_Tight           = new basicHists("g55_Tight"     ,       wk(), 2.8, m_detailLevel);
  h_g55_TightIso        = new basicHists("g55_TightIso"  ,       wk(), 2.8, m_detailLevel);

  h_g65_loose           = new basicHists("g65_loose"     ,       wk(), 2.8, m_detailLevel);
  h_g65_Tight           = new basicHists("g65_Tight"     ,       wk(), 2.8, m_detailLevel);
  h_g65_TightIso        = new basicHists("g65_TightIso"  ,       wk(), 2.8, m_detailLevel);

  h_g75_loose           = new basicHists("g75_loose"     ,       wk(), 2.8, m_detailLevel);
  h_g75_Tight           = new basicHists("g75_Tight"     ,       wk(), 2.8, m_detailLevel);
  h_g75_TightIso        = new basicHists("g75_TightIso"  ,       wk(), 2.8, m_detailLevel);

  
  return EL::StatusCode::SUCCESS;
}


EL::StatusCode ProcessTrigStudy :: setupJob (EL::Job& job)
{

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode ProcessTrigStudy :: histInitialize ()
{
  Info("histInitialize()", "Calling histInitialize \n");

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode ProcessTrigStudy :: fileExecute ()
{
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode ProcessTrigStudy :: changeInput (bool firstFile)
{
  // Here you do everything you need to do when we change input files,
  // e.g. resetting branch addresses on trees.  If you are using
  // D3PDReader or a similar service this method is not needed.
  if(firstFile){
    if ( this->configure() == EL::StatusCode::FAILURE ) {
      Error("initialize()", "Failed to properly configure. Exiting." );
      return EL::StatusCode::FAILURE;
    }

    TFile* inputFile = wk()->inputFile();
    TIter next(inputFile->GetListOfKeys());
    TKey *key;
    while ((key = (TKey*)next())) {
      std::string keyName = key->GetName();

      std::size_t found = keyName.find("cutflow");
      bool foundCutFlow = (found!=std::string::npos);

      found = keyName.find("weighted");
      bool foundWeighted = (found!=std::string::npos);

      if(foundCutFlow){
	
	if(m_useWeighted && foundWeighted){
	  cout << "Getting NSample events from " << keyName << endl;
	  m_sampleEvents = ((TH1F*)key->ReadObj())->GetBinContent(1);
	  cout << "Setting Sample events to: " << m_sampleEvents << endl;
	  cout << "Setting Lumi  to: " << m_lumi << endl;
	  
	  std::string inFileName = inputFile->GetName();
	  std::size_t foundJZ5Pos = inFileName.find("JZ5");
	  bool foundJZ5 = (foundJZ5Pos!=std::string::npos);
	  if(foundJZ5){
	    m_sampleEvents = (m_sampleEvents - 352219559.60316455) ;
	    cout << "Correcting Sample events to: " << m_sampleEvents << endl;
	  }
	  
	} else if(!m_useWeighted && !foundWeighted) {
	  cout << "Getting NSample events from " << keyName << endl;
	  m_sampleEvents = ((TH1F*)key->ReadObj())->GetBinContent(1);
	  cout << "Setting Sample events to: " << m_sampleEvents << endl;
	  cout << "Setting Lumi  to: " << m_lumi << endl;
	}

      }//Found CF
    }//over Keys
    
  }// first file

  TTree *tree = wk()->tree();
  tree->SetBranchStatus ("*", 0);

  tree->SetBranchStatus  ("runNumber",    1);
  tree->SetBranchAddress ("runNumber",    &m_runNumber);

  tree->SetBranchStatus  ("eventNumber",    1);
  tree->SetBranchAddress ("eventNumber",    &m_eventNumber);

  tree->SetBranchStatus  ("lumiBlock",    1);
  tree->SetBranchAddress ("lumiBlock",    &m_lumiBlock);

  tree->SetBranchStatus  ("NPV",    1);
  tree->SetBranchAddress ("NPV",    &m_NPV);

  tree->SetBranchStatus  ("actualInteractionsPerCrossing",    1);
  tree->SetBranchAddress ("actualInteractionsPerCrossing",    &m_actualInteractionsPerCrossing);

  tree->SetBranchStatus  ("averageInteractionsPerCrossing",    1);
  tree->SetBranchAddress ("averageInteractionsPerCrossing",    &m_averageInteractionsPerCrossing);

  tree->SetBranchStatus  ("weight_pileup", 1);
  tree->SetBranchAddress ("weight_pileup", &m_weight_pileup);

  if(m_detailLevel > 0){
    tree->SetBranchStatus  ("jet_Timing", 1);
    tree->SetBranchAddress ("jet_Timing", &m_jet_Timing);

    tree->SetBranchStatus  ("jet_GhostMuonSegmentCount", 1);
    tree->SetBranchAddress ("jet_GhostMuonSegmentCount", &m_jet_GhostMuonSegmentCount);
  }

  tree->SetBranchStatus  ("jet_clean_passLooseBad", 1);
  tree->SetBranchAddress ("jet_clean_passLooseBad", &m_jet_clean_passLooseBad);

  tree->SetBranchStatus  ("passedTriggers", 1);
  tree->SetBranchAddress ("passedTriggers", &m_passedTriggers);

  tree->SetBranchStatus  ("weight", 1);
  tree->SetBranchAddress ("weight", &m_weight);

  tree->SetBranchStatus  ("weight_xs", 1);
  tree->SetBranchAddress ("weight_xs", &m_weight_xs);


  tree->SetBranchStatus  ("jet_pt", 1);
  tree->SetBranchAddress ("jet_pt", &m_jet_pt);

  tree->SetBranchStatus  ("jet_eta", 1);
  tree->SetBranchAddress ("jet_eta", &m_jet_eta);

  tree->SetBranchStatus  ("jet_phi", 1);
  tree->SetBranchAddress ("jet_phi", &m_jet_phi);

  tree->SetBranchStatus  ("jet_E", 1);
  tree->SetBranchAddress ("jet_E", &m_jet_E);

  std::string photonName = "ph";

  tree->SetBranchStatus  ((photonName+"_pt" ).c_str(), 1);
  tree->SetBranchAddress ((photonName+"_pt" ).c_str(), &m_gamma_pt);

  tree->SetBranchStatus  ((photonName+"_eta").c_str(), 1);
  tree->SetBranchAddress ((photonName+"_eta").c_str(), &m_gamma_eta);

  tree->SetBranchStatus  ((photonName+"_phi").c_str(), 1);
  tree->SetBranchAddress ((photonName+"_phi").c_str(), &m_gamma_phi);
  
  tree->SetBranchStatus  ((photonName+"_E"  ).c_str(), 1);
  tree->SetBranchAddress ((photonName+"_E"  ).c_str(), &m_gamma_E);

  tree->SetBranchStatus   ((photonName+"_isIsolated_Cone40CaloOnly").c_str(), 1);
  tree->SetBranchAddress  ((photonName+"_isIsolated_Cone40CaloOnly").c_str(),  &m_gamma_isIsolated_Cone40CaloOnly );

  tree->SetBranchStatus   ((photonName+"_isIsolated_Cone40"        ).c_str(), 1);
  tree->SetBranchAddress  ((photonName+"_isIsolated_Cone40"        ).c_str(),  &m_gamma_isIsolated_Cone40         );

  tree->SetBranchStatus   ((photonName+"_isIsolated_Cone20"        ).c_str(), 1);
  tree->SetBranchAddress  ((photonName+"_isIsolated_Cone20"        ).c_str(),  &m_gamma_isIsolated_Cone20         );

  tree->SetBranchStatus   ((photonName+"_IsLoose"                  ).c_str(), 1);
  tree->SetBranchAddress  ((photonName+"_IsLoose"                  ).c_str(),  &m_gamma_IsLoose                   );

  tree->SetBranchStatus   ((photonName+"_IsMedium"                 ).c_str(), 1);
  tree->SetBranchAddress  ((photonName+"_IsMedium"                 ).c_str(),  &m_gamma_IsMedium                  );

  tree->SetBranchStatus   ((photonName+"_IsTight"                  ).c_str(), 1);
  tree->SetBranchAddress  ((photonName+"_IsTight"                  ).c_str(),  &m_gamma_IsTight                   );

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode ProcessTrigStudy :: initialize ()
{
  m_eventCounter = -1;

  if(m_applyGRL){
    m_grl = new GoodRunsListSelectionTool("GoodRunsListSelectionTool");
    std::vector<std::string> vecStringGRL;
    m_GRLxml = PathResolverFindCalibFile( m_GRLxml.c_str() );
    vecStringGRL.push_back(m_GRLxml);
    ANA_CHECK(m_grl->setProperty( "GoodRunsListVec", vecStringGRL));
    ANA_CHECK(m_grl->setProperty("PassThrough", false));
    ANA_CHECK(m_grl->initialize());
  }

  Info("initialize()", "Succesfully initialized! \n");
  return EL::StatusCode::SUCCESS;
}


EL::StatusCode ProcessTrigStudy :: execute ()
{
  // Here you do everything that needs to be done on every single
  // event, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.
  //if(m_debug) Info("execute()", "Processing Event");
  ++m_eventCounter;

  wk()->tree()->GetEntry (wk()->treeEntry());
  unsigned njets       = m_jet_pt->size();
  unsigned ngamma      = m_gamma_pt->size();

  if(ngamma < 1){
    if(m_debug) cout << "skipping event with no photons " << endl;
    return EL::StatusCode::SUCCESS;    
  }

  if(m_doData){
   
    if ( m_applyGRL ) {
      if ( !m_grl->passRunLB( m_runNumber, m_lumiBlock ) ) {
	if(m_debug) cout << "GRL:: Fail Event " << endl;
        return EL::StatusCode::SUCCESS; // go to next event
      }else{
	//if(m_debug) cout << "GRL:: Pass Event " << endl;
      }
    }
  }


  //
  // doing cleaning
  //
  bool  passCleaning   = true;
  for(unsigned int i = 0;  i< njets; ++i){
    if(m_jet_pt->at(i) > 50){
      if(fabs(m_jet_eta->at(i)) < 2.8){
	if(!m_doCleaning && !m_jet_clean_passLooseBad->at(i)){
	  //cout << "Skipping jet " << endl;
	  continue;
	}

	if(!m_jet_clean_passLooseBad->at(i)) passCleaning = false;
      }
    }else{
      break;
    }
  }

  if(m_debug) cout << " Pass All Cut " << endl;
  float eventWeight = m_weight;
  
  if(!m_doData && m_doPUReweight) eventWeight *= m_weight_pileup;

  if(m_doData) eventWeight = 1.0;

  if(m_useWeighted && (eventWeight > 1000)){
    cout << "skipping event with Weight: " << eventWeight << " " << m_lumi << " " << m_weight << " " << m_weight_xs << endl;
    return EL::StatusCode::SUCCESS;    
  }

  //
  //  Jet Cleaning 
  //
  if(m_doCleaning && !passCleaning){
    //if(!passCleaning){
    cout << "Fail Cleaning " << endl;
    return EL::StatusCode::SUCCESS;
  }
  
  if(m_debug) cout << " Make EventData " << endl;
  eventData thisEvent = eventData(m_runNumber, m_eventNumber, 
				  m_jet_pt, m_jet_eta, m_jet_phi, m_jet_E,  m_jet_Timing, m_jet_GhostMuonSegmentCount,
				  m_gamma_pt, m_gamma_eta, m_gamma_phi, m_gamma_E,  
				  m_gamma_isIsolated_Cone40CaloOnly,    m_gamma_isIsolated_Cone40,    m_gamma_isIsolated_Cone20,
				  m_gamma_IsLoose, m_gamma_IsMedium,  m_gamma_IsTight,
				  m_NPV, m_averageInteractionsPerCrossing, m_averageInteractionsPerCrossing, 
				  eventWeight);
  if(m_debug) cout << " Made EventDAta " << endl;				  
  


  hIncl->Fill(thisEvent);


  if(m_debug) Info("execute()", "Doing Trigger ");

  bool m_dumpTrig = false ;
  if(m_dumpTrig){
    cout << " --------" << endl;
    for(std::string& thisTrig: *m_passedTriggers)
      cout << thisTrig << endl;
  }

  bool passHLT_g35_loose = passTrig("HLT_g35_loose_L1EM15");
  if(passHLT_g35_loose){

    gammaData& leadPhoton = thisEvent.gammas.at(0);
    float leadPhotonPt = leadPhoton.pt;
    bool  is_tight     = leadPhoton.IsTight;
    bool  is_iso       = leadPhoton.IsIsoCone20;

    h_g35_loose->Fill(thisEvent);
    if(is_tight)           h_g35_Tight->Fill(thisEvent);
    if(is_tight && is_iso) h_g35_TightIso->Fill(thisEvent);

    if(leadPhotonPt > 45){
      h_g45_loose->Fill(thisEvent);
      if(is_tight)           h_g45_Tight->Fill(thisEvent);
      if(is_tight && is_iso) h_g45_TightIso->Fill(thisEvent);
    }

    if(leadPhotonPt > 55){
      h_g55_loose->Fill(thisEvent);
      if(is_tight)           h_g55_Tight->Fill(thisEvent);
      if(is_tight && is_iso) h_g55_TightIso->Fill(thisEvent);
    }
    
    if(leadPhotonPt > 65){
      h_g65_loose->Fill(thisEvent);
      if(is_tight)           h_g65_Tight->Fill(thisEvent);
      if(is_tight && is_iso) h_g65_TightIso->Fill(thisEvent);
    }

    if(leadPhotonPt > 75){
      h_g75_loose->Fill(thisEvent);
      if(is_tight)           h_g75_Tight->Fill(thisEvent);
      if(is_tight && is_iso) h_g75_TightIso->Fill(thisEvent);
    }


  }
  
  //bool passHLT_j360 = (find(m_passedTriggers->begin(), m_passedTriggers->end(), "HLT_j360" ) != m_passedTriggers->end());

  return EL::StatusCode::SUCCESS;
}







EL::StatusCode ProcessTrigStudy :: postExecute ()
{
  // Here you do everything that needs to be done after the main event
  // processing.  This is typically very rare, particularly in user
  // code.  It is mainly used in implementing the NTupleSvc.
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode ProcessTrigStudy :: finalize ()
{

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode ProcessTrigStudy :: histFinalize ()
{
  return EL::StatusCode::SUCCESS;
}


bool ProcessTrigStudy::passTrig(std::string trigName){
  return (find(m_passedTriggers->begin(), m_passedTriggers->end(), trigName ) != m_passedTriggers->end());
}
