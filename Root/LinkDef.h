/* Plotting Tools */
#include <ZprimeDM/ZprimeAlgorithm.h>
#include <ZprimeDM/TruthPhotonJetOR.h>
#include <ZprimeDM/JetPartonHistsAlgo.h>
#include <ZprimeDM/ZprimeMGTrijetAlgo.h>
#include <ZprimeDM/ZprimeMGTrijetPickAlgo.h>
#include <ZprimeDM/ZprimeHistsBaseAlgo.h>
#include <ZprimeDM/ZprimeGammaJetJetHistsAlgo.h>
#include <ZprimeDM/ZprimeDijetHistsAlgo.h>
#include <ZprimeDM/ZprimeTrijetHistsAlgo.h>
#include <ZprimeDM/DiphotonHistsAlgo.h>
#include <ZprimeDM/GammaJetHistsAlgo.h>
#include <ZprimeDM/ZprimeNtuplerContainer.h>
#include <ZprimeDM/ZprimeNtupler.h>
#include <ZprimeDM/ZprimeNtuplerNew.h>
#include <ZprimeDM/ZprimeJetHistsAlgo.h>
#include <ZprimeDM/JetStudiesAlgo.h>
#include <ZprimeDM/SortAlgo.h>
#include <ZprimeDM/MiniTreeEventSelection.h>
#include <ZprimeDM/ProcessTrigStudy.h>

/* Particle */
#include <ZprimeDM/Particle.h>
#include <ZprimeDM/Jet.h>
#include <ZprimeDM/FatJet.h>
#include <ZprimeDM/JetConstituent.h>
#include <ZprimeDM/TrackParticle.h>
#include <ZprimeDM/Photon.h>
#include <ZprimeDM/TruthParticle.h>

#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;

#pragma link C++ class ZprimeAlgorithm+;
#pragma link C++ class TruthPhotonJetOR+;
#pragma link C++ class JetPartonHistsAlgo+;
#pragma link C++ class ZprimeMGTrijetAlgo+;
#pragma link C++ class ZprimeMGTrijetPickAlgo+;
#pragma link C++ class ZprimeHistsBaseAlgo+;
#pragma link C++ class ZprimeGammaJetJetHistsAlgo+;
#pragma link C++ class ZprimeDijetHistsAlgo+;
#pragma link C++ class ZprimeTrijetHistsAlgo+;
#pragma link C++ class DiphotonHistsAlgo+;
#pragma link C++ class GammaJetHistsAlgo+;
#pragma link C++ class ZprimeNtupler+;
#pragma link C++ class ZprimeNtuplerNew+;
#pragma link C++ class ZprimeNtuplerContainer+;
#pragma link C++ class ZprimeJetHistsAlgo+;
#pragma link C++ class JetStudiesAlgo+;
#pragma link C++ class SortAlgo+;
#pragma link C++ class MiniTreeEventSelection+;
#pragma link C++ class ProcessTrigStudy+;

#pragma link C++ class ZprimeDM::Particle+;
#pragma link C++ class ZprimeDM::Jet+;
#pragma link C++ class ZprimeDM::FatJet+;
#pragma link C++ class ZprimeDM::JetConstituent+;
#pragma link C++ class ZprimeDM::TrackParticle+;
#pragma link C++ class ZprimeDM::Photon+;
#pragma link C++ class ZprimeDM::TruthParticle+;

#endif
