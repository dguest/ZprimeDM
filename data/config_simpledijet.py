import ROOT
from xAODAnaHelpers import Config

commonsel={"m_mc"                     : args.is_MC,
           "m_debug"                  : False,
           "m_jetDetailStr"           : "kinematic energy clean trackPV",
           "m_doPUReweight"           : True,
           "m_doCleaning"             : True,
           "m_jetPtCleaningCut"       : 25,
           "m_doTrigger"              : True,
           }

c = Config()

GRL = "GoodRunsLists/data15_13TeV/data15_13TeV.periodAllYear_DetStatus-v73-pro19-08_DQDefects-00-01-02_PHYS_StandardGRL_All_Good.xml"

#
# Process Ntuple
#

c.setalg("MiniTreeEventSelection", { "m_name"                   : "",
                                     "m_debug"                  : False,
                                     "m_mc"                     : args.is_MC,
                                     "m_GRLxml"                 : GRL,
                                     "m_applyGRL"               : not args.is_MC,
                                     "m_doTruthOnly"            : False,
                                     "m_triggerDetailStr"       : "passTriggers",
                                     "m_jetDetailStr"           : "kinematic clean"
                                     } )



dijet_list = {"HLT_j15"  : ( 25,  40),
              "HLT_j25"  : ( 40,  60),
              "HLT_j35"  : ( 60,  85),
              "HLT_j60"  : ( 85, 145),
              "HLT_j110" : (145, 220),
              "HLT_j175" : (220, 330),
              "HLT_j260" : (330, 410),
              "HLT_j360" : (410,  -1),
              }

for trigger in dijet_list:
    
    print() 
    print("Adding...",trigger)
    print("  Pt range...",trigger,dijet_list[trigger][0],"...",trigger,dijet_list[trigger][1])
    print()

    dijet12 = commonsel.copy()
    dijet12.update({"m_name"           : "dijet12_"+trigger,
                    "m_debug"          : False,
                    "m_resoJet0Idx"    : 0,
                    "m_resoJet1Idx"    : 1,
                    "m_nJets"          : 2,
                    "m_minJetPt"       : 25,
                    "m_reso0PtCut"     : dijet_list[trigger][0],
                    "m_reso0PtCutMax"  : dijet_list[trigger][1],
                    "m_reso1PtCut"     : 25,
                    "m_YStarCut"       : 0.6,
                    "m_trigger"        : trigger,
                    })
    c.setalg("ZprimeDijetHistsAlgo", dijet12 )
