import ROOT
from xAODAnaHelpers import Config

doDetails=True
histDetail   = "" #2d debug"
jetDetail    = "kinematic clean useTheS" if args.is_MC else "kinematic clean"
photonDetail = "kinematic effSF"
trigJetDetail= "kinematic useTheS" if args.is_MC else "kinematic"

c = Config()

commonsel={"m_mc"                     : args.is_MC,
           "m_debug"                  : False,
           "m_doDetails"              : doDetails,
           "m_histDetailStr"          : histDetail,
           "m_jetDetailStr"           : jetDetail,
           "m_photonDetailStr"        : photonDetail,
           "m_doPUReweight"           : args.is_MC,
           "m_doCleaning"             : True,
           "m_jetPtCleaningCut"       : 25,
           "m_doTrigger"              : True
           #"m_phTrigMatch"            : True
           }

#
# Process Ntuple
#

c.setalg("MiniTreeEventSelection", { "m_name"                   : "",
                                     "m_debug"                  : False,
                                     "m_mc"                     : args.is_MC,
                                     "m_doPUreweighting"        : False,
                                     "m_applyGRL"               : False,
                                     "m_doTruthOnly"            : False,
                                     "m_triggerDetailStr"       : "passTriggers",
                                     "m_jetDetailStr"           : jetDetail,
                                     "m_photonDetailStr"        : photonDetail,
                                     "m_trigJetDetailStr"       : trigJetDetail,
                                     } )

triggerlist=[('HLT_g140_loose','HLT_g120_loose',False)]
if args.is_MC:
    triggerlist.append(('HLT_g75_tight_3j25noL1_L1EM22VHI','HLT_g60_loose',True))
else:
    triggerlist.append(('HLT_g75_tight_3j50noL1_L1EM22VHI','HLT_g60_loose',False))

for trigger,refTrigger,emutrigjet in triggerlist:
    for photonPt in [25,85]:
        for jet0Pt in [25,65]:
            for jet1Pt in [25,65]:
                if jet0Pt<jet1Pt: continue
                jetStr='2j%d'%jet0Pt if jet0Pt==jet1Pt else 'j%d_j%d'%(jet0Pt,jet1Pt)

                config=commonsel.copy()
                config.update({"m_name"                   : "dijetgamma_g%d_%s_%s"%(photonPt,jetStr,trigger.replace('3j25','3j50')),
                               "m_doBCIDCheck"            : not args.is_MC,
                               "m_trigger"                : '%s,%s'%(trigger,refTrigger),
                               "m_emuTrigJet"             : emutrigjet,
                               "m_reso0PtCut"             : jet0Pt,
                               "m_reso1PtCut"             : jet1Pt,
                               "m_minPhotonPt"            : photonPt})
                c.setalg("ZprimeGammaJetJetHistsAlgo", config )

                config=commonsel.copy()
                config.update({"m_name"                   : "dijetgamma_g%d_%s_%s_ref"%(photonPt,jetStr,trigger.replace('3j25','3j50')),
                               "m_doBCIDCheck"            : not args.is_MC,
                               "m_trigger"                : '%s'%(refTrigger),
                               "m_reso0PtCut"             : jet0Pt,
                               "m_reso1PtCut"             : jet1Pt,
                               "m_minPhotonPt"            : photonPt})
                c.setalg("ZprimeGammaJetJetHistsAlgo", config )
