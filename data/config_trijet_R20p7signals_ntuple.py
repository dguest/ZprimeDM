import ROOT
from xAODAnaHelpers import Config
from ZprimeDM import commonconfigR20p7 as commonconfig
import datetime

date=datetime.datetime.now().strftime("%Y%m%d")

c = Config()

jetDetailStr="kinematic clean energy layer trackPV flavTag sfFTagFix%s"%(''.join(['%d'%btagWP for btagWP in commonconfig.btagWPs]))
if args.is_MC: jetDetailStr+=" truth"

commonconfig.apply_common_config(c,
                                 isMC=args.is_MC,isAFII=args.is_AFII,
                                 triggerSelection='L1_J[0-9]*|L1_[0-9]J[0-9]*|HLT_j[0-9]*.*|HLT_noalg_J[0-9]*|HLT_[0-9]j*.*|HLT_ht[0-9]*.*',
                                 doJets=True,btagWPs=commonconfig.btagWPs,
                                 doPhotons=False)

containers=ROOT.vector('ZprimeNtuplerContainer')()
containers.push_back(ROOT.ZprimeNtuplerContainer(ROOT.ZprimeNtuplerContainer.JET   ,'SignalJets'   ,'jet',jetDetailStr                          ,'kinematic clean'))

c.setalg("ZprimeNtupler",          { "m_name"                : "AnalysisAlgo",
                                     "m_inputAlgo"           : "SignalJets_Algo",
                                     "m_containers"          : containers,
                                     "m_eventDetailStr"      : "pileup",
                                     "m_trigDetailStr"       : "passTriggers passTrigBits"
                                     } )
