import ROOT
from xAODAnaHelpers import Config
from ZprimeDM import commonconfig

isSys=False
doDetails=True
histDetail   = "" #2d" #2d debug"
jetDetail    = "kinematic clean layer trackPV energy flavTag sfFTagHyb%s"%(''.join(['%d'%btagWP for btagWP in commonconfig.btagWPs]))
photonDetail = "kinematic isolation purity PID effSF"
nameSuffix=""
if args.treeName!="outTree":
    isSys=True
    doDetails=False

    histDetail  =""
    jetDetail   ="kinematic clean"
    photonDetail="kinematic"

    nameSuffix="/sys"+args.treeName[7:]

c = Config()

commonsel={"m_mc"                     : args.is_MC,
           "m_doDetails"              : doDetails,
           "m_histDetailStr"          : histDetail,
           "m_jetDetailStr"           : jetDetail,
           "m_photonDetailStr"        : photonDetail,
           "m_doPUReweight"           : True,
           "m_doCleaning"             : True,
           "m_jetPtCleaningCut"       : 25,
           "m_doTrigger"              : True
           }

#
# Process Ntuple
#

c.setalg("MiniTreeEventSelection", { "m_name"                   : "",
                                     "m_mc"                     : args.is_MC,
                                     "m_applyGRL"               : False,
                                     "m_doPUreweighting"        : False,
                                     "m_doTruthOnly"            : False,
                                     "m_triggerDetailStr"       : "passTriggers",
                                     "m_jetDetailStr"           : jetDetail,
                                     "m_photonDetailStr"        : photonDetail
                                     } )

triggers=[]
triggers.append(('HLT_g60_loose',85,(25,)))
triggers.append(('HLT_g75_tight_3j50noL1_L1EM22VHI' if not args.is_MC else 'HLT_g60_loose',85,(65,)))
triggers.append(('HLT_g140_loose',150,(25,65,)))

for trigger, photonPt, jetPts in triggers:
    for jetPt in jetPts:
        for btagWP in [0,77]: #,70,77,85]:
            commonsel['m_trigger']    =trigger
            commonsel['m_reso0PtCut'] =jetPt;
            commonsel['m_reso1PtCut'] =jetPt;
            commonsel['m_reso0Btag']  =btagWP
            commonsel['m_reso1Btag']  =btagWP
            commonsel['m_minPhotonPt']=photonPt

            basename='dijetgamma_g%d_2j%d'%(photonPt,jetPt)
            if(btagWP!=0): basename+='_btag%d'%btagWP

            config=commonsel.copy()
            config.update({"m_name"                   : "%s%s"%(basename,nameSuffix),
                           "m_doBCIDCheck"            : not args.is_MC,
                           "m_ystarCut"               : 0.8,
                           "m_dRISRclosejCut"         : 0.85})
            c.setalg("ZprimeGammaJetJetHistsAlgo", config )

            config=commonsel.copy()
            config.update({"m_name"                   : "%s_noprw%s"%(basename,nameSuffix),
                           "m_doPUReweight"           : False,
                           "m_trigger"                : trigger,
                           "m_ystarCut"               : 0.8,
                           "m_dRISRclosejCut"         : 0.85,
                           "m_mjjCut"                 : 160})
            c.setalg("ZprimeGammaJetJetHistsAlgo", config )

            config=commonsel.copy()
            config.update({"m_name"                   : "%s_nomjj%s"%(basename,nameSuffix),
                           "m_doBCIDCheck"            : not args.is_MC,
                           "m_ystarCut"               : 0.8,
                           "m_dRISRclosejCut"         : 0.85})
            c.setalg("ZprimeGammaJetJetHistsAlgo", config )

            config=commonsel.copy()
            config.update({"m_name"                   : "%s_nocuts%s"%(basename,nameSuffix),
                           "m_ystarCut"               : 99})
            c.setalg("ZprimeGammaJetJetHistsAlgo", config )

            config=commonsel.copy()
            config.update({"m_name"                   : "%s_ystar%s"%(basename,nameSuffix),
                           "m_ystarCut"               : 0.8})
            c.setalg("ZprimeGammaJetJetHistsAlgo", config )

            config=commonsel.copy()
            config.update({"m_name"                   : "%s_ystar_dRISRclosej%s"%(basename,nameSuffix),
                           "m_ystarCut"               : 0.8,
                           "m_dRISRclosejCut"         : 0.85})
            c.setalg("ZprimeGammaJetJetHistsAlgo", config )

            config=commonsel.copy()
            config.update({"m_name"                   : "%s_dijetystar%s"%(basename,nameSuffix),
                           "m_ystarCut"               : 0.6})
            c.setalg("ZprimeGammaJetJetHistsAlgo", config )
