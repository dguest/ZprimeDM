import ROOT
from xAODAnaHelpers import Config
from ZprimeDM import commonconfig
import datetime

date=datetime.datetime.now().strftime("%Y%m%d")

c = Config()

commonconfig.apply_common_config(c,
                                 isMC=args.is_MC,
                                 isAFII=args.is_AFII,
                                 triggerSelection='HLT_g[0-9]+_loose|HLT_g75_tight_3j[0-9]+noL1_L1EM22VHI|HLT_g[0-9]+_tight',
                                 #triggerSelection='HLT_g140_loose|HLT_g75_tight_3j50noL1_L1EM22VHI',
                                 doJets=True,
                                 doPhotons=True)

SelectPhotons=commonconfig.findAlgo(c,"SelectPhotons")
SelectPhotons.m_pT_min=20

c.setalg("TrigMatcher",  { "m_inContainerName": "SignalPhotons",
                           #"m_trigChains"     : "HLT_g60_loose,HLT_g120_loose,HLT_g140_loose,HLT_g75_tight_3j50noL1_L1EM22VHI" } )
                           "m_trigChains"     : "HLT_g140_loose,HLT_g75_tight_3j50noL1_L1EM22VHI,HLT_g75_tight_3j25noL1_L1EM22VHI" } )

containers=ROOT.vector('ZprimeNtuplerContainer')()
#containers.push_back(ROOT.ZprimeNtuplerContainer(ROOT.ZprimeNtuplerContainer.PHOTON,'SignalPhotons','phTrig' ,'kinematic isolation purity PID effSF'))
containers.push_back(ROOT.ZprimeNtuplerContainer(ROOT.ZprimeNtuplerContainer.JET   ,'HLT_xAOD__JetContainer_a4tcemsubjesFS','jetTrig','kinematic'                           ))
containers.push_back(ROOT.ZprimeNtuplerContainer(ROOT.ZprimeNtuplerContainer.PHOTON,'SignalPhotons'                        ,'ph'     ,'kinematic isolation purity PID effSF'))
containers.push_back(ROOT.ZprimeNtuplerContainer(ROOT.ZprimeNtuplerContainer.JET   ,'SignalJets'                           ,'jet'    ,'kinematic clean'                     ))


c.setalg("ZprimeNtupler",          { "m_eventDetailStr"          : "pileup",
                                     "m_trigDetailStr"           : "passTriggers",
                                     "m_containers"              : containers
                                     } )
