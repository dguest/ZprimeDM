import ROOT
from xAODAnaHelpers import Config, utils
if utils.is_release21():
    from ZprimeDM import commonconfig
else:
    from ZprimeDM import commonconfigR20p7 as commonconfig
import datetime

date=datetime.datetime.now().strftime("%Y%m%d")

c = Config()

jetDetailStr="kinematic clean energy layer trackPV flavTag sfFTagHyb%s"%(''.join(['%d'%btagWP for btagWP in commonconfig.btagWPs]))
if args.is_MC: jetDetailStr+=" truth"

commonconfig.apply_common_config(c,
                                 isMC=args.is_MC,isAFII=args.is_AFII,
                                 triggerSelection='HLT_g[0-9]+_loose|HLT_g75_tight_3j50noL1_L1EM22VHI|HLT_g75_tight_3j50noL1_L1EM22VHI|HLT_g75_tight_3j[0-9]+noL1_L1EM22VHI|HLT_g[0-9]+_tight',
                                 doJets=True,btagWPs=commonconfig.btagWPs,
                                 doPhotons=True)

SelectPhotons=commonconfig.findAlgo(c,"SelectPhotons")
SelectPhotons.m_pT_min=30

containers=ROOT.vector('ZprimeNtuplerContainer')()
containers.push_back(ROOT.ZprimeNtuplerContainer(ROOT.ZprimeNtuplerContainer.PHOTON,'SignalPhotons','ph' ,'kinematic isolation purity PID effSF','kinematic'))
containers.push_back(ROOT.ZprimeNtuplerContainer(ROOT.ZprimeNtuplerContainer.JET   ,'SignalJets'   ,'jet',jetDetailStr                          ,'kinematic clean'))

c.setalg("ZprimeNtupler",          { "m_name"                : "AnalysisAlgo",
                                     "m_inputAlgo"           : "ORAlgo_Syst",
                                     "m_containers"          : containers,
                                     "m_eventDetailStr"      : "pileup",
                                     "m_trigDetailStr"       : "passTriggers passTrigBits"
                                     } )
