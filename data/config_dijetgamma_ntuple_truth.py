import ROOT
from xAODAnaHelpers import Config

c = Config()

c.setalg("BasicEventSelection",          { "m_truthLevelOnly"      : True,
                                           "m_useMetaData"         : False
                                           } )

c.setalg("SortAlgo",              { "m_inContainerName"         :  "AntiKt4TruthJets",
                                    "m_outContainerName"        :  "AntiKt4TruthJetsSort"
                                    } )

c.setalg("SortAlgo",              { "m_inContainerName"         :  "TruthPhotons",
                                    "m_outContainerName"        :  "TruthPhotonsSort"
                                    } )

c.setalg("JetSelector",                  { "m_inContainerName"         :  "AntiKt4TruthJetsSort",
                                           "m_outContainerName"        :  "SignalJets",
                                           "m_decorateSelectedObjects" :  False, 
                                           "m_createSelectedContainer" :  True, 
                                           "m_pT_min"                  :  25e3,
                                           "m_eta_max"                 :  2.8,
                                           "m_pass_min"                :  2
                                           } )

c.setalg("TruthSelector",                { "m_inContainerName"         :  "TruthPhotonsSort",
                                           "m_outContainerName"        :  "SignalPhotons",
                                           "m_decorateSelectedObjects" :  False,
                                           "m_createSelectedContainer" :  True,
                                           "m_pT_min"                  :  25e3,
                                           "m_eta_max"                 :  2.5,
                                           "m_pass_min"                :  1
                                           } )

c.setalg("TruthPhotonJetOR",             { "m_jetContainerName"    : "SignalJets",
                                           "m_photonContainerName" : "SignalPhotons",
                                           "m_outJetContainerName" : "SignalJetswoPhoton",
                                           "m_minDR"               : 0.2
                                           } )

containers=ROOT.vector('ZprimeNtuplerContainer')()
containers.push_back(ROOT.ZprimeNtuplerContainer(ROOT.ZprimeNtuplerContainer.JET   ,'SignalJetswoPhoton'   ,'jet','kinematic'))
containers.push_back(ROOT.ZprimeNtuplerContainer(ROOT.ZprimeNtuplerContainer.TRUTH ,'SignalPhotons'        ,'ph','kinematic'))

c.setalg("ZprimeNtupler",                { "m_truthLevelOnly" : True,
                                           "m_containers"     : containers
                                           } )
