import ROOT
from xAODAnaHelpers import Config

isSys=False
doDetails=True
histDetail="" #2d" #2d debug"
jetDetail ="kinematic clean layer trackPV energy"
nameSuffix=""
if args.treeName!="outTree":
    isSys=True
    doDetails=False
    histDetail = ""
    jetDetail ="kinematic clean"
    nameSuffix="/sys"+args.treeName[7:]

c = Config()

commonsel={"m_mc"                     : args.is_MC,
           "m_doDetails"              : doDetails,
           "m_histDetailStr"          : histDetail,
           "m_jetDetailStr"           : jetDetail,
           "m_doPUReweight"           : True,
           "m_doCleaning"             : True,
           "m_jetPtCleaningCut"       : 25,
           "m_doTrigger"              : True
           }

#
# Process Ntuple
#

c.setalg("MiniTreeEventSelection", { "m_name"                   : "MiniTreeEventSelection",
                                     "m_mc"                     : args.is_MC,
                                     "m_applyGRL"               : False,
                                     "m_doPUreweighting"        : False,
                                     "m_doTruthOnly"            : False,
                                     "m_triggerDetailStr"       : "passTriggers",
                                     "m_jetDetailStr"           : jetDetail
                                     } )

selections=[]
selections.append(('HLT_j380',440,''))
selections.append(('HLT_j175',220,''))
selections.append(('HLT_3j200',50,'_3j200'))
#selections.append(('HLT_3j175',50,'_3j175'))
selections.append(('HLT_ht1000_L1J100',50,'_ht1000'))
selections.append(('HLT_j100_2j55_bmv2c2060_split',50,'_j100_2j55_bmv2c2060'))
selections.append(('HLT_2j55_bmv2c2060_split_ht300_L14J15',50,'_2j55_bmv2c2060_split_ht300'))

for trigger,jetPt,tag in selections:
    trijet23=commonsel.copy()
    trijet23.update({"m_name"            : "trijet_j{0}_2j25{1}".format(jetPt,nameSuffix+tag),
                     "m_trigger"         : trigger,
                     "m_reso0PtCut"      : 25,
                     "m_reso1PtCut"      : 25,
                     "m_minLeadingJetPt" : jetPt,
                     "m_ystarCut"        : 0.6,
                     "m_mjjCut"          : 300})
    c.setalg("ZprimeTrijetHistsAlgo", trijet23 )

    trijet23_nomjj=commonsel.copy()
    trijet23_nomjj.update({"m_name"            : "trijet_j{0}_2j25_nomjj{1}".format(jetPt,nameSuffix+tag),
                           "m_trigger"         : trigger,
                           "m_reso0PtCut"      : 25,
                           "m_reso1PtCut"      : 25,
                           "m_minLeadingJetPt" : jetPt,
                           "m_ystarCut"        : 0.6})
    c.setalg("ZprimeTrijetHistsAlgo", trijet23_nomjj )

    trijet23_nocuts=commonsel.copy()
    trijet23_nocuts.update({"m_name"            : "trijet_j{0}_2j25_nocuts{1}".format(jetPt,nameSuffix+tag),
                            "m_trigger"         : trigger,
                           "m_reso0PtCut"      : 25,
                           "m_reso1PtCut"      : 25,
                           "m_minLeadingJetPt" : jetPt})
    c.setalg("ZprimeTrijetHistsAlgo", trijet23_nocuts )


    trijet23_ystar=commonsel.copy()
    trijet23_ystar.update({"m_name"            : "trijet_j{0}_2j25_ystar{1}".format(jetPt,nameSuffix+tag),
                           "m_trigger"         : trigger,
                           "m_reso0PtCut"      : 25,
                           "m_reso1PtCut"      : 25,
                           "m_minLeadingJetPt" : jetPt,
                           "m_ystarCut"        : 0.6})
    c.setalg("ZprimeTrijetHistsAlgo", trijet23_ystar )

#    dijet12=commonsel.copy()
#    dijet12.update({"m_name"        : "dijet12_j%d_j25%s"%(jetPt,nameSuffix),
#                    "m_trigger"     : trigger,
#                    "m_resoJet0Idx" : 0,
#                    "m_resoJet1Idx" : 1,
#                    "m_nJets"       : 2,
#                    "m_minJetPt"    : 25,
#                    "m_reso0PtCut"  : jetPt,
#                    "m_reso1PtCut"  : 25,
#                    "m_ystarCut"    : 0.6})
#    c.setalg("ZprimeDijetHistsAlgo", dijet12 )
#
#    dijet13=commonsel.copy()
#    dijet13.update({"m_name"        : "dijet13_j%d_j25%s"%(jetPt,nameSuffix),
#                    "m_trigger"     : trigger,
#                    "m_resoJet0Idx" : 0,
#                    "m_resoJet1Idx" : 2,
#                    "m_nJets"       : 3,
#                    "m_minJetPt"    : 25,
#                    "m_reso0PtCut"  : jetPt,
#                    "m_reso1PtCut"  : 25})
#    c.setalg("ZprimeDijetHistsAlgo", dijet13 )

#
# Prescaled list
#dijet_list = {"HLT_j15"  : ( 25,  40),
#              "HLT_j25"  : ( 40,  60),
#              "HLT_j35"  : ( 60,  85),
#              "HLT_j60"  : ( 85, 145),
#              "HLT_j110" : (145, 220),
#              "HLT_j175" : (220, 330),
#              "HLT_j260" : (330, 440),
#              "HLT_j380" : (440,  -1),
#              }
#for trigger,ptrange in dijet_list.items():
#    dijet12 = commonsel.copy()
#    dijet12.update({"m_name"           : "dijet12_"+trigger,
#                    "m_trigger"        : trigger,
#                    "m_resoJet0Idx"    : 0,
#                    "m_resoJet1Idx"    : 1,
#                    "m_nJets"          : 2,
#                    "m_minJetPt"       : 25,
#                    "m_reso0PtCut"     : ptrange[0],
#                    "m_reso0PtCutMax"  : ptrange[1],
#                    "m_reso1PtCut"     : 25,
#                    "m_ystarCut"       : 0.6,
#                    })
#    c.setalg("ZprimeDijetHistsAlgo", dijet12 )
