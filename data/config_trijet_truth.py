import ROOT
from xAODAnaHelpers import Config

commonsel={"m_debug"                  : False,
           "m_mc"                     : True,
           "m_histDetailStr"          : "", #"2d debug",
           "m_jetDetailStr"           : "kinematic",
           "m_doDetails"              : True,
           "m_doCleaning"             : False,
           "m_doTrigger"              : False,
           }


c = Config()

#
# Process Ntuple
#

c.setalg("MiniTreeEventSelection", { "m_name"                   : "",
                                     "m_debug"                  : False,
                                     "m_mc"                     : True,
                                     "m_applyGRL"               : False,
                                     "m_doTruthOnly"            : True,
                                     "m_jetDetailStr"           : "kinematic"
                                     } )

for jetPt in [220,440]:
    #
    # Trijet
    trijet_2j25=commonsel.copy()
    trijet_2j25.update({ "m_name"             : "trijet_j%d_2j25"%jetPt,
                         "m_reso0PtCut"       : 25,
                         "m_reso1PtCut"       : 25,
                         "m_minLeadingJetPt"  : jetPt,
                         "m_ystarCut"         : 0.6,
                         "m_mjjCut"           : 300})
    c.setalg("ZprimeTrijetHistsAlgo", trijet_2j25 )

    trijet_2j25_nomjj=commonsel.copy()
    trijet_2j25_nomjj.update({ "m_name"             : "trijet_j%d_2j25_nomjj"%jetPt,
                               "m_reso0PtCut"       : 25,
                               "m_reso1PtCut"       : 25,
                               "m_minLeadingJetPt"  : jetPt,
                               "m_ystarCut"         : 0.6
                               })
    c.setalg("ZprimeTrijetHistsAlgo", trijet_2j25_nomjj )

    trijet_2j25_nocuts=commonsel.copy()
    trijet_2j25_nocuts.update( { "m_name"                   : "trijet_j%d_2j25_nocuts"%jetPt,
                                 "m_reso0PtCut"             : 25,
                                 "m_reso1PtCut"             : 25,
                                 "m_minLeadingJetPt"        : jetPt
                                 })
    c.setalg("ZprimeTrijetHistsAlgo", trijet_2j25_nocuts )

    trijet_2j25_ystar=commonsel.copy()
    trijet_2j25_ystar.update({"m_name"            : "trijet_j%d_2j25_ystar"%jetPt,
                              "m_reso0PtCut"      : 25,
                              "m_reso1PtCut"      : 25,
                              "m_minLeadingJetPt" : jetPt,
                              "m_ystarCut"        : 0.6})
    c.setalg("ZprimeTrijetHistsAlgo", trijet_2j25_ystar )


    #
    # Dijet-Trijet
    dijet13_j25=commonsel.copy()
    dijet13_j25.update({ "m_name"        : "dijet13_j%d_j25"%jetPt,
                         "m_resoJet0Idx" : 0,
                         "m_resoJet1Idx" : 2,
                         "m_nJets"       : 3,
                         "m_minJetPt"    : 25,
                         "m_reso0PtCut"  : jetPt,
                         "m_reso1PtCut"  : 25
                         })
    c.setalg("ZprimeDijetHistsAlgo", dijet13_j25 )

    #
    # Dijet
    dijet12_j25=commonsel.copy()
    dijet12_j25.update({ "m_name"                   : "dijet12_j%d_j25"%jetPt,
                         "m_resoJet0Idx"            : 0,
                         "m_resoJet1Idx"            : 1,
                         "m_nJets"                  : 2,
                         "m_minJetPt"               : 25,
                         "m_reso0PtCut"             : jetPt,
                         "m_reso1PtCut"             : 25,
                         "m_ystarCut"               : 0.6
                         })
    c.setalg("ZprimeDijetHistsAlgo", dijet12_j25 )


dijet12_2j25=commonsel.copy()
dijet12_2j25.update({ "m_name"        : "dijet12_2j25",
                      "m_resoJet0Idx" : 0,
                      "m_resoJet1Idx" : 1,
                      "m_nJets"       : 2,
                      "m_minJetPt"    : 25,
                      "m_reso0PtCut"  : 25,
                      "m_reso1PtCut"  : 25,
                      "m_ystarCut"    : 0.6
                      })
c.setalg("ZprimeDijetHistsAlgo", dijet12_2j25 )

#
# Prescaled list
dijet_list = {"HLT_j15"  : ( 25,  40),
              "HLT_j25"  : ( 40,  60),
              "HLT_j35"  : ( 60,  85),
              "HLT_j60"  : ( 85, 145),
              "HLT_j110" : (145, 220),
              "HLT_j175" : (220, 330),
              "HLT_j260" : (330, 440),
              "HLT_j380" : (440,  -1),
              }
for trigger,ptrange in dijet_list.items():
    dijet12 = commonsel.copy()
    dijet12.update({"m_name"           : "dijet12_"+trigger,
                    "m_trigger"        : trigger,
                    "m_resoJet0Idx"    : 0,
                    "m_resoJet1Idx"    : 1,
                    "m_nJets"          : 2,
                    "m_minJetPt"       : 25,
                    "m_reso0PtCut"     : ptrange[0],
                    "m_reso0PtCutMax"  : ptrange[1],
                    "m_reso1PtCut"     : 25,
                    "m_ystarCut"       : 0.6,
                    })
    c.setalg("ZprimeDijetHistsAlgo", dijet12 )
