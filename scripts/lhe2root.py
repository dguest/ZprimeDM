#! /usr/bin/env python

# Jim Henderson January 2013
# James.Henderson@cern.ch
#
# Usage:
# lhe2root.py <input_file.lhe> <OPTIONAL: output_file_name.root>
#
# PLEASE NOTE: This conversion was generated to convert les houches 1.0, it may not work on other versions
#              Please check the les houches version # at the top of the .lhe file

import os, sys
import ROOT

import xml.etree.ElementTree as ET

# Get the input lhe file
if len(sys.argv) < 2:
    print("\nYou must enter the .lhe file you wish to convert as the first arguement. Exiting \n")
    sys.exit(1)

try:
    input_file = sys.argv[1]
    tree = ET.parse(input_file)
    root = tree.getroot()
except:
    print("\nThe entered file cannot be opened, please enter a vaild .lhe file. Exiting. \n")
    sys.exit(1)
    pass

if len(sys.argv) > 2:    output_file_name = sys.argv[2]
else:                    output_file_name = "lhe.root"

try:    output_file = ROOT.TFile(output_file_name, "RECREATE")
except:
    print("Cannot open output file named: " + output_file_name + "\nPlease enter a valid output file name as the 2nd arguement. Exiting")
    sys.exit(1)
    pass

# Setup the weight information
MetaData_EventCount=ROOT.TH1D("MetaData_EventCount_Test","MetaData_EventCount", 6, 0.5, 6.5)
MetaData_EventCount.GetXaxis().SetBinLabel(1, "nEvents initial");
MetaData_EventCount.GetXaxis().SetBinLabel(2, "nEvents selected");
MetaData_EventCount.GetXaxis().SetBinLabel(3, "sumOfWeights initial");
MetaData_EventCount.GetXaxis().SetBinLabel(4, "sumOfWeights selected");
MetaData_EventCount.GetXaxis().SetBinLabel(5, "sumOfWeightsSquared initial");
MetaData_EventCount.GetXaxis().SetBinLabel(6, "sumOfWeightsSquared selected");

cutflow=ROOT.TH1D("cutflow_Test","cutflow", 1, 0.5, 1.5)
cutflow.SetCanExtend(ROOT.TH1.kAllAxes)
cutflow.GetXaxis().FindBin("all")

cutflow_weighted=ROOT.TH1D("cutflow_weighted_Test","cutflow_weighted", 1, 0.5, 1.5)
cutflow_weighted.SetCanExtend(ROOT.TH1.kAllAxes)
cutflow_weighted.GetXaxis().FindBin("all")

# Setup the tree
output_tree = ROOT.TTree("Physics", "Physics")
print("Setup complete \nOpened file " + str(sys.argv[1]) + "  \nConverting to .root format and outputing to " + output_file_name)

# Setup output branches
br_pdgId = ROOT.vector('Int_t')()
br_status= ROOT.vector('Int_t')()
br_parents  = ROOT.vector('std::vector<Int_t>')()
parents  = ROOT.vector('Int_t')()

br_pt = ROOT.vector('Float_t')()
br_eta= ROOT.vector('Float_t')()
br_phi= ROOT.vector('Float_t')()
br_E  = ROOT.vector('Float_t')()
br_m  = ROOT.vector('Float_t')()

# Create a struct which acts as the TBranch for non-vectors
ROOT.gROOT.ProcessLine( "struct MyStruct { \
                          Int_t runNumber; \
                          Long_t eventNumber; \
                          Int_t mcEventNumber; \
                          Int_t mcChannelNumber; \
                          Float_t mcEventWeight; \
                          Float_t weight; \
                          Float_t weight_xs; \
                          Int_t nmc; \
                         };")

# Assign the variables to the struct
s = ROOT.MyStruct()

output_tree.Branch("runNumber",       ROOT.AddressOf(s, 'runNumber')      , "runNumber/I");
output_tree.Branch("eventNumber",     ROOT.AddressOf(s, 'eventNumber')    , "eventNumber/LI");
output_tree.Branch("mcEventNumber",   ROOT.AddressOf(s, 'mcEventNumber')  , "mcEventNumber/I");
output_tree.Branch("mcChannelNumber", ROOT.AddressOf(s, 'mcChannelNumber'), "mcChannelNumber/I");
output_tree.Branch("mcEventWeight",   ROOT.AddressOf(s, 'mcEventWeight')  , "mcEventWeight/F");

output_tree.Branch("weight",    ROOT.AddressOf(s, 'weight')   , "weight/F");
output_tree.Branch("weight_xs", ROOT.AddressOf(s, 'weight_xs'), "weight_xs/F");

output_tree.Branch("nmc",   ROOT.AddressOf(s, 'nmc')  , "nmc/I");

output_tree.Branch("mc_pdgId" ,br_pdgId)
output_tree.Branch("mc_status",br_status)
output_tree.Branch("mc_parents", br_parents)
output_tree.Branch("mc_pt" ,br_pt)
output_tree.Branch("mc_eta",br_eta)
output_tree.Branch("mc_phi",br_phi)
output_tree.Branch("mc_E"  ,br_E)
output_tree.Branch("mc_m"  ,br_m)

#
# Prepare generator info
MGGenerationInfo=root.find('header').find('MGGenerationInfo')
xsecStr=MGGenerationInfo.text.strip().split('\n')[1]
s.weight_xs=float(xsecStr.split()[-1])
nEvents=len(root.findall('event'))

s.runNumber=0
s.eventNumber=0
s.mcEventNumber=0
s.mcChannelNumber=0
s.mcEventWeight=0
s.weight=0
s.nmc=0
for event in root.findall('event'):
    if s.eventNumber%10000==0: print(s.eventNumber)
    lines=event.text.strip().split('\n')
    infoStr=lines[0]
    particlesStr=lines[1:]

    s.weight=float(infoStr.split()[2])*nEvents
    s.mcEventWeight=s.weight/s.weight_xs

    # MC particles
    for pStr in particlesStr:
        s.nmc+=1

        parts=pStr.split()
        br_pdgId.push_back(int(parts[0]))
        br_status.push_back(int(parts[1]))

        parents.clear()
        for i in [int(parts[2]),int(parts[3])]:
            if i==0: continue
            parents.push_back(i-1)
        br_parents.push_back(parents)

        p4=ROOT.TLorentzVector(*[float(x) for x in parts[6:10]])
        br_pt.push_back(p4.Pt())
        br_eta.push_back(p4.Eta() if p4.Pt()!=0 else 0.)
        br_phi.push_back(p4.Phi())
        br_E.push_back(p4.E())
        br_m.push_back(p4.M())
        
    # Write the event
    output_tree.Fill()

    MetaData_EventCount.Fill(1, 1) # initialNevents
    MetaData_EventCount.Fill(2, 1) # finalNevents
    MetaData_EventCount.Fill(3, s.mcEventWeight) # initialSumW
    MetaData_EventCount.Fill(4, s.mcEventWeight) # finalSumW
    MetaData_EventCount.Fill(5, s.mcEventWeight*s.mcEventWeight) # initialSumWSquared
    MetaData_EventCount.Fill(6, s.mcEventWeight*s.mcEventWeight) # finalSumWSquared

    cutflow.Fill(1, 1)
    cutflow_weighted.Fill(1, 1)

    s.mcEventNumber+=1
    s.eventNumber+=1
    #if s.mcEventNumber==1000: break

    # Reset variables
    s.mcEventWeight = 0.
    s.weight = 0.
    s.nmc = 0
    br_pdgId.clear()
    br_status.clear()
    br_parents.clear()
    br_pt.clear()
    br_eta.clear()
    br_phi.clear()
    br_E.clear()
    br_m.clear()

output_tree.Write()
MetaData_EventCount.Write()
cutflow.Write()
cutflow_weighted.Write()
output_file.Close()
