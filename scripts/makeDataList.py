#!/usr/bin/env python

import argparse
import xml.etree.ElementTree

from rucio.client import Client

parser = argparse.ArgumentParser(description="Make data using GRL and latest tag.")
parser.add_argument('GRL',help="GRL corresponding to requested runs.")
parser.add_argument('-d','--derivation',default='AOD',help="Derivation to look for.")
parser.add_argument('-s','--stream'    ,default='physics_Main',help="Stream to process.")
parser.add_argument('-y','--year',default='16',help="Year (20YY) to search for.")
args = parser.parse_args()

tree = xml.etree.ElementTree.parse(args.GRL)
root = tree.getroot()
runlist=root.find('./NamedLumiRange/Metadata[@Name="RunList"]')
runs=[int(run) for run in runlist.text.split(',')]

client=Client()

for run in runs:
    if args.year=='15':
        datasets=client.list_dids('data15_13TeV',{'name':'data15_13TeV.%08d.%s.merge.%s.*r7562*'%(run,args.stream,args.derivation)},type='container')
    else:
        #print('data16_13TeV.%08d.%s.merge.%s.f*'%(run,args.stream,args.derivation))
        datasets=client.list_dids('data16_13TeV',{'name':'data16_13TeV.%08d.%s.merge.%s.*f*'%(run,args.stream,args.derivation)}) #,type='container')
    #print([dataset for dataset in datasets],'data16_13TeV.%08d.physics_Main.merge.%s.f*'%(run,args.derivation))
    latest_ds=None
    latest_tag=None
    for dataset in datasets:
        if 'tid' in dataset: continue
        tag=dataset.split('.')[-1]
        if tag==None or tag>latest_tag:
            latest_ds=dataset
            latest_tag=tag

    if latest_ds==None:
        print('Missing dataset for run %d'%run)
    else:
        print(latest_ds)

