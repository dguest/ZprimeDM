import ROOT
from xAODAnaHelpers import Config

c = Config()


doOverlap = False

#
# Basic Setup 
#
triggers = "HLT_g.*|HLT_e.*"
c.setalg("BasicEventSelection",  { "m_name"                : "basicEventSel",
                                   "m_applyGRLCut"         : False,
                                   "m_derivationName"      : "EXOT1", 
                                   "m_storePassL1"         : True,
                                   "m_storePassHLT"        : True,
                                   "m_storeTrigDecisions"  : True,
                                   "m_storeTrigKeys"       : True,
                                   "m_applyTriggerCut"     : True,
                                   "m_triggerSelection"    : triggers, 
                                   "m_doPUreweighting"     : False, 
                                   #"m_lumiCalcFileNames"   : "MultijetAlgo/ilumicalc_histograms_None_267073-271048.root",
                                   "m_lumiCalcFileNames"   : "MultijetAlgo/ilumicalc_histograms_None_276262-284484.root",
                                   "m_PRWFileNames"        : "MultijetAlgo/Multijet_PRW_25ns.root",
                                   #"m_PRWFileNames"        : "MultijetAlgo/Multijet_PRW.root",
                                   "m_PU_default_channel"  : 302590
                                   } )

#
#  Jet Calibration
#
c.setalg("JetCalibrator", { "m_name"                   : "jetCalib_AntiKt4TopoEM", 
                            "m_inContainerName"        : "AntiKt4EMTopoJets",
                            "m_jetAlgo"                : "AntiKt4EMTopo",
                            "m_outContainerName"       : "AntiKt4EMTopoJets_Calib", 
                            "m_outputAlgo"             : "AntiKt4EMTopoJets_Calib_Algo",
                            "m_sort"                   : True,
                            "m_saveAllCleanDecisions"  : True,
                            "m_calibConfigAFII"        : "JES_MC15Prerecommendation_AFII_June2015.config",
                            "m_calibConfigFullSim"     : "JES_data2016_data2015_Recommendation_Dec2016_rel21.config",
                            "m_calibConfigData"        : "JES_data2016_data2015_Recommendation_Dec2016_rel21.config",
                            "m_calibSequence"          : "JetArea_Residual_EtaJES_GSC",
                            "m_JESUncertConfig"        : "JES_2016/Moriond2017/JES2016_SR_Scenario1.config",
                            "m_JESUncertMCType"        : "MC15",
                            "m_setAFII"                : False,
                            "m_JERUncertConfig"        : "JetResolution/Prerec2015_xCalib_2012JER_ReducedTo9NP_Plots_v2.root",
                            "m_JERApplyNominal"        : False,
                            "m_redoJVT"                : True,
                            "m_systName"               : "Nominal",
                            "m_systVal"                : 0,
                            } )


#
#  Jet Selection
#
c.setalg("JetSelector", { "m_name"                    :  "JetSelector",
                          "m_inContainerName"         :  "AntiKt4EMTopoJets_Calib",
                          "m_inputAlgo"               :  "AntiKt4EMTopoJets_Calib_Algo",
                          "m_outContainerName"        :  "Jets_Select",
                          "m_outputAlgo"              :  "Jets_Select_Algo",
                          "m_decorateSelectedObjects" :  False, 
                          "m_createSelectedContainer" :  True, 
                          "m_cleanJets"               :  False, 
                          "m_pT_min"                  :  25e3,
                          "m_eta_max"                 :  2.8,
                          "m_useCutFlow"              :  True,
                          "m_doJVT"                   :  True,
                          "m_jetScaleType"            :  "JetEMScaleMomentum",
                          "m_debug"                   :  False,
                          } )


#
# Photon Calibrator
#
c.setalg("PhotonCalibrator",   { "m_inContainerName"         : "Photons",
                                 "m_outContainerName"        : "Photons_Calib",
                                 "m_outputAlgoSystNames"     : "Photons_Calib_Algo",
                                 "m_esModel"                 : "es2015PRE",
                                 "m_decorrelationModel"      : "1NP_v1",
                                 } )


#
# Photon Selector
#
c.setalg("PhotonSelector",   { "m_name"                    : "PhotonSelector",
                               "m_inContainerName"         : "Photons_Calib",
                               "m_outContainerName"        : "Photons_Select",
                               "m_inputAlgoSystNames"      : "Photons_Calib_Algo",
                               "m_outputAlgoSystNames"     : "Photons_Select_Algo",
                               "m_createSelectedContainer" : True,
                               "m_pT_min"                  : 25*1000,
                               "m_eta_max"                 : 2.5,
                               "m_pass_min"                : 1,
                               "m_vetoCrack"               : True,
                               "m_photonIdCut"             : "Loose",
                               "m_doAuthorCut"             : True,
                               } )

if doOverlap:
    c.setalg("OverlapRemover", { "m_inContainerName_Jets"      : "Jets_Select",
                                 "m_inputAlgoJets"             : "Jets_Select_Algo",
                                 "m_outContainerName_Jets"     : "SignalJets",
                                 "m_outputAlgoJets"            : "SignalJets_Algo",
                                 "m_inContainerName_Photons"   : "Photons_Select",
                                 "m_inputAlgoPhotons"          : "Photons_Select_Algo",
                                 "m_outContainerName_Photons"  : "SignalPhotons",
                                 "m_outputAlgoPhotons"         : "SignalPhotons_Algo",
                                 "m_createSelectedContainers"  : True
                                 } )
                                         

inJetName    = "SignalJets"
inPhotonName = "SignalPhotons_Algo"
if not doOverlap: 
    inJetName    = "Jets_Select"
    inPhotonName = "Photons_Select"

c.setalg("ZprimeNtupler",          { "m_jetContainerName"          : inJetName,
                                     "m_photonContainerName"       : inPhotonName,
                                     "m_useCutFlow"                : True, 
                                     "m_writeTree"                 : True,
                                     "m_truthLevelOnly"            : False,
                                     "m_photonDetailStr"           : "kinematic isolation PID",
                                     "m_jetDetailStr"              : "kinematic rapidity clean energy trackPV",
                                     "m_trigDetailStr"             : "passTriggers basic menuKeys", 
                                     "m_debug"                     : False,
                                     } )
