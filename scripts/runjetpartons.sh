#!/bin/bash

function abortError {
    echo "FAILED ${1}"
    exit -1
}

if [ ${#} != 1 ]; then
    echo "usage: ${0} direct/condor"
    exit -1
fi
MODE=${1}

if [ "x${MODE}x" == "xcondorx" ]; then
    RUNCODE="condor --optCondorWait --optFilesPerWorker 10"
else
    RUNCODE="direct"
fi

FILES_TRIJET="ZprimeDM/filelists/MC15.99995*.MGPy8EG_dmA_dijet_Np*_mR*_mDM10000_gSM0p16_gDM1p50.TRUTH1.txt"
FILES_TRIJET_XPTJ="ZprimeDM/filelists/MC15.99992*.MGPy8EG_dmA_dijet_Np*_Min*_mR*_mDM10000_gSM0p16_gDM1p50.TRUTH1.txt"
FILES_TRIJET_NOMERG="ZprimeDM/filelists/MC15.99994*.MGPy8EG_dmA_dijet_Np*_mR*_mDM10000_gSM0p16_gDM1p50.TRUTH1.txt"
FILES_TRIJET_NOMERG_XPTJ="ZprimeDM/filelists/MC15.99991*.MGPy8EG_dmA_dijet_Np*_Min*_mR*_mDM10000_gSM0p16_gDM1p50.TRUTH1.txt"
FILES_JETJET="ZprimeDM/filelists/MC15.999999.MGPy8EG_jetjet_Par*.TRUTH1.txt"
FILES_JETJETJET="ZprimeDM/filelists/MC15.999998.MGPy8EG_jetjetjet_Par*.TRUTH1.txt"

./xAODAnaHelpers/scripts/xAH_run.py --config ZprimeDM/data/config_jet_partons.py --files ${FILES_TRIJET} ${FILES_TRIJET_XPTJ} ${FILES_TRIJET_NOMERG} ${FILES_TRIJET_NOMERG_XPTJ} ${FILES_JETJET} ${FILES_JETJETJET} --inputList --submitDir OUT_jetpartons --force ${RUNCODE} || abortError OUT_jetpartons
