#!/bin/bash

function abortError {
    echo "FAILED ${1}"
    exit -1
}

if [ ${#} != 1 ]; then
    echo "usage: ${0} direct/condor/grid"
    exit -1
fi
MODE=${1}

echo $TYPE
echo $EXOT2
echo ${FILES_JETJET//TYPE/EXOT2}

#
# Declare options for running with different drivers
#  RUNCODE = For running on offical samples
#  RUNCODECONT = For running on private rucio containers (needed for grid to get rid of user.${USER} prefix in outname
#
if [ "x${MODE}x" == "xcondorx" ]; then
    RUNCODE="condor --optFilesPerWorker 5 --optBatchWait"
    RUNCODECONT=${RUNCODE}
elif [ "x${MODE}x" == "xgridx" ]; then
    TAG=20170624-02
    RUNCODE="prun --optGridDestSE=NERSC_LOCALGROUPDISK  --optGridOutputSampleName=user.%nickname%.%in:name[1]%.%in:name[2]%.%in:name[3]%.SELECTION.${TAG}/" # --optSubmitFlags=\"--useNewCode\""
    RUNCODECONT="prun --optGridDestSE=NERSC_LOCALGROUPDISK  --optGridOutputSampleName=user.%nickname%.%in:name[3]%.%in:name[4]%.SELECTION.${TAG}/ --optSubmitFlags=\"--allowTaskDuplication\""
else
    RUNCODE="--nevents 1000 direct"
    RUNCODECONT=${RUNCODE}
fi

#
# List of input dataset lists
# String TYPE will be replaced by the necessary DxAOD name
FILES_GAMMAJETJET="ZprimeDM/filelists/Signal_dijetgamma.TYPE.list ZprimeDM/filelists/Sherpa_gammajet.TYPE.list"
FILES_JETJET="ZprimeDM/filelists/Signal_trijet.TYPE.list ZprimeDM/filelists/Pythia8_jetjet.TYPE.list ZprimeDM/filelists/Pythia8_jetjet_JZ.TYPE.list ZprimeDM/filelists/Sherpa_jetjet.TYPE.list"

FILES_DATA="ZprimeDM/filelists/data15.TYPE.list ZprimeDM/filelists/data16.TYPE.list"


#
# Prepare output directory
TAG=$(date +%Y%m%d)-${MODE}
DATADIR=$(mktemp -d --suffix ${USER}_ZPrimeDM_${TAG})

#
# Run!
#

#
# Truths
#./xAODAnaHelpers/scripts/xAH_run.py --config ZprimeDM/data/config_dijet_ntuple_truth.py       --files ZprimeDM/filelists/Pythia8_jetjet_tla.TRUTH3.list --inputList --inputRucio --isMC --submitDir ${DATADIR}/OUT_dijet_ntuple_truth       --force ${RUNCODE/SELECTION/dijet}      || abortError OUT_dijet_ntuple_truth

#./xAODAnaHelpers/scripts/xAH_run.py --config ZprimeDM/data/config_gammajetjet_ntuple_truth.py --files ZprimeDM/filelists/Sherpa_gammajet.TRUTH3.list    --inputList --inputRucio --isMC --submitDir ${DATADIR}/OUT_gammajetjet_ntuple_truth --force ${RUNCODE/SELECTION/gammajetjet} || abortError OUT_gammajetjet_ntuple_truth

#
# Full sim
echo "./xAODAnaHelpers/scripts/xAH_run.py --config ZprimeDM/data/config_dijet_ntuple.py         --files ${FILES_JETJET//TYPE/EXOT2}           --inputList --inputRucio --isMC          --submitDir ${DATADIR}/OUT_dijet_ntuple              --force ${RUNCODE/SELECTION/dijet}             || abortError OUT_dijet_ntuple"
#./xAODAnaHelpers/scripts/xAH_run.py --config ZprimeDM/data/config_dijet_ntuple.py         --files ${FILES_DATA//TYPE/EXOT2.cont}        --inputList --inputRucio                 --submitDir ${DATADIR}/OUT_dijet_ntuple_data         --force ${RUNCODECONT/SELECTION/dijet}         || abortError OUT_dijet_ntuple_data
#./xAODAnaHelpers/scripts/xAH_run.py --config ZprimeDM/data/config_gammajetjet_ntuple.py   --files ${FILES_GAMMAJETJET_AF2}              --inputList --inputRucio --isMC --isAFII --submitDir ${DATADIR}/OUT_gammajetjet_ntuple_afii   --force ${RUNCODE/SELECTION/gammajetAFII} || abortError OUT_gammajetjet_ntuple_afii
#./xAODAnaHelpers/scripts/xAH_run.py --config ZprimeDM/data/config_gammajetjet_ntuple.py   --files ${FILES_GAMMAJETJET//TYPE/EXOT6}      --inputList --inputRucio --isMC          --submitDir ${DATADIR}/OUT_gammajetjet_ntuple        --force ${RUNCODE/SELECTION/gammajet}          || abortError OUT_gammajetjet_ntuple
#./xAODAnaHelpers/scripts/xAH_run.py --config ZprimeDM/data/config_gammajetjet_ntuple.py   --files ${FILES_DATA//TYPE/EXOT6.cont}        --inputList --inputRucio                 --submitDir ${DATADIR}/OUT_gammajetjet_ntuple_data    --force ${RUNCODECONT/SELECTION/gammajet}     || abortError OUT_gammajetjet_ntuple_data

#
# Trigger studies
#./xAODAnaHelpers/scripts/xAH_run.py --config ZprimeDM/data/config_photontrigger_ntuple.py   --files ${FILES_GAMMAJETJET//TYPE/JETM4} --inputList --inputRucio --isMC          --submitDir ${DATADIR}/OUT_photontrigger_ntuple         --force ${RUNCODE/SELECTION/photontrigger}     || abortError OUT_photontrigger_ntuple
#./xAODAnaHelpers/scripts/xAH_run.py --config ZprimeDM/data/config_photontrigger_ntuple.py   --files ${FILES_DATA//TYPE/JETM4.cont}   --inputList --inputRucio                 --submitDir ${DATADIR}/OUT_photontrigger_ntuple_data    --force ${RUNCODECONT/SELECTION/photontrigger} || abortError OUT_photontrigger_ntuple_data

#
# Clean-up
rm -rf ${DATADIR}
