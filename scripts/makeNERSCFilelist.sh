#!/bin/bash

if [ ${#} != 1 ]; then
    echo "usage: ${0} dslist"
    exit -1
fi

dslist=${1}

for dsname in $(cat ${dslist})
do
    dsname=$(basename ${dsname})
    rucio list-file-replicas ${dsname}/ --rse NERSC_LOCALGROUPDISK --protocol srm | awk '{print $12}' | grep SFN | sed -r s_".*SFN=(.*)"_"\1"_g > ZprimeFilelists/filelists/${dsname}.txt
done
