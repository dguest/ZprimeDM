trap 'echo "KILL" ${RUNLIST}; for pid in ${RUNLIST}; do pkill -P ${pid}; done' EXIT

if [ ! -d logs ]; then
    mkdir logs
fi

function runOne {
    CONFIG=${1}    
    OUTNAME=${2}
    EXTRA=${3}
    FILELIST=${@:4}
    runone.sh -d ${DATADIR} ${EXTRA} ${MODE} ${CONFIG} ${OUTNAME} ${FILELIST} > logs/${OUTNAME}.out 2> logs/${OUTNAME}.err &

    NEWPID=${!}
    RUNLIST="${RUNLIST} ${NEWPID}"
    echo "${OUTNAME} PID = ${NEWPID}"
}

function merge {
    OUTNAME=${1}
    OUTROOT=${2}
    FILELIST=${@:3}

    hadd -n 0 -f ${DATADIR}/OUT_${OUTNAME}/${OUTROOT} ${DATADIR}/OUT_${OUTNAME}/${FILELIST} || exit -1
}

function mergeSys {
    OUTNAME=${1}
    SYSLIST=${@:2}

    if [ -e ${DATADIR}/OUT_${OUTNAME} ]; then
	rm -rf ${DATADIR}/OUT_${OUTNAME}
    fi
    mkdir ${DATADIR}/OUT_${OUTNAME}

    for path in ${DATADIR}/OUT_${OUTNAME}_nominal/hist-*
    do
	name=$(basename ${path})

	SYSFILES=""
	ROOTRM=""
	for sys in ${SYSLIST}
	do
	    SYSFILES="${SYSFILES} ${DATADIR}/OUT_${OUTNAME}_sys${sys}/${name}"
	    ROOTRM="${ROOTRM} ${DATADIR}/OUT_${OUTNAME}_sys${sys}/${name}:/cutflow"
	    ROOTRM="${ROOTRM} ${DATADIR}/OUT_${OUTNAME}_sys${sys}/${name}:/cutflow_weighted"
	done
	if [ "x${ROOTRM}x" != "xx" ]; then rootrm.py ${ROOTRM}; fi

	hadd ${DATADIR}/OUT_${OUTNAME}/${name} ${path} ${SYSFILES} > logs/haddsys_${OUTNAME}.out 2> logs/haddsys_${OUTNAME}.err &
    done
    wait
}

if [ ${#} != 1 ]; then
    echo "usage: ${0} direct/condor/gengine"
    exit -1
fi
# configuration
MODE=${1}
if [ -z ${DATADIR} ]; then
    DATADIR=$(pwd)
fi
