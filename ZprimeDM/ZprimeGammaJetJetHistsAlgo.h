#ifndef ZprimeDM_ZprimeGammaJetJetHistsAlgo_H
#define ZprimeDM_ZprimeGammaJetJetHistsAlgo_H

// algorithm wrapper
#include <ZprimeDM/ZprimeHistsBaseAlgo.h>

#include <ZprimeDM/PhotonTriggerHists.h>

class ZprimeGammaJetJetHistsAlgo : public ZprimeHistsBaseAlgo
{
public:
  double m_minPhotonPt;     // minimum photon pt
  uint m_doBarrelEndCap;    // include barrel (bit 0)/endcap (bit 1) photons
  bool m_phTrigMatch;       // photon must be matched to a trigger object
  bool m_emuTrigJet;        // Emulate 50 GeV cut on the trigger jets
  bool m_emuTrigJetOverlap; // Require that one of the trigger jets overlaps with the photon

protected:
  virtual void initISRCutflow();
  virtual bool doISRCutflow();

  virtual EL::StatusCode histInitialize ();
  virtual EL::StatusCode histFill (float eventWeight);
  virtual EL::StatusCode histFinalize ();

public:
  // this is a standard constructor
  ZprimeGammaJetJetHistsAlgo ();

private:
  //
  // cutflow
  int m_cf_njets;
  int m_cf_nphotons;
  int m_cf_photon;
  int m_cf_phTrigMatch;
  int m_cf_emuTrigJet;
  int m_cf_emuTrigJetOverlap;
  int m_cf_barrelendcap;

  //
  // histograms
  DijetISRHists *hSysPhotonIDup; //!
  DijetISRHists *hSysPhotonIDdw; //!

  PhotonTriggerHists *hPhotonTrigger; //!

  // this is needed to distribute the algorithm to the workers
  ClassDef(ZprimeGammaJetJetHistsAlgo, 1);
};

#endif
