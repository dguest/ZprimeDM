#ifndef ZprimeDM_ZprimeJetHistsAlgo_H
#define ZprimeDM_ZprimeJetHistsAlgo_H

#include <EventLoop/StatusCode.h>
#include <EventLoop/Algorithm.h>
#include <EventLoop/Worker.h>

//algorithm wrapper
#include "xAODAnaHelpers/Algorithm.h"

// our histogramming code
#include <ZprimeDM/DijetISREvent.h>

#include <ZprimeDM/CutflowHists.h>
#include <ZprimeDM/DijetISRHists.h>

// ROOT include(s):
#include "TH1D.h"
#include "TH2D.h"
#include "TProfile.h"
#include "TLorentzVector.h"

#include <sstream>
#include <vector>

using namespace std;



class ZprimeJetHistsAlgo : public xAH::Algorithm
{
  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.
public:
  
  //configuration variables
  bool m_debug;
  bool m_mc;

  // switches
  std::string m_jetDetailStr;
  std::string m_photonDetailStr;
  bool m_doTruthOnly;
  bool m_doPUReweight;           
  bool m_doCleaning;
  float m_jetPtCleaningCut;

  // trigger config
  bool m_doTrigger;  
  bool m_dumpTrig;
  std::string m_trigger;

  // Kinematic selection
  //None

private:

  //
  // Cutflow
  int m_cf_trigger;
  int m_cf_cleaning;
  int m_cf_jet;

  //
  // Triggers
  std::vector<std::string> m_triggers;

  //
  // Histograms
  ZprimeDM::JetHists *h_jet0; //!

  // variables that don't get filled at submission time should be
  // protected from being send from the submission node to the worker
  // node (done by the //!)

protected:
  // Cutflow data
  CutflowHists *m_cutflow; //!

  // Event data
  DijetISREvent *m_event; //!

public:
  // Tree *myTree; //!
  // TH1 *myHist; //!

  // this is a standard constructor
  ZprimeJetHistsAlgo ();

  // these are the functions inherited from Algorithm
  virtual EL::StatusCode histInitialize ();
  virtual EL::StatusCode initialize ();
  virtual EL::StatusCode execute ();
  virtual EL::StatusCode histFinalize ();

  // this is needed to distribute the algorithm to the workers
  ClassDef(ZprimeJetHistsAlgo, 1);
};

#endif
