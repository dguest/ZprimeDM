#ifndef ZprimeDM_JetPartonHistsAlgo_H
#define ZprimeDM_JetPartonHistsAlgo_H

#include <ZprimeDM/ZprimeResonanceHists.h>
#include <ZprimeDM/ZprimeISRHists.h>
#include <ZprimeDM/ZprimeTruthHists.h>
#include <ZprimeDM/PartonJetHists.h>

#include <xAODAnaHelpers/JetHists.h>

// algorithm wrapper
#include <ZprimeDM/ZprimeAlgorithm.h>

class JetPartonHistsAlgo : public ZprimeAlgorithm
{
  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.
public:

  std::string m_jetContainerName;    // JetContainerName - name of the input container with jets to plot
  std::string m_truthContainerName;  // TruthContainerName - name of the input container with truth particles

private:
  JetHists         *m_jethists;         //!
  JetHists         *m_jetshists[10];    //!
  PartonJetHists   *m_partonjethists;   //!
  PartonJetHists   *m_partonjethists_0; //!

  // variables that don't get filled at submission time should be
  // protected from being send from the submission node to the worker
  // node (done by the //!)
public:
  // Tree *myTree; //!
  // TH1 *myHist; //!



  // this is a standard constructor
  JetPartonHistsAlgo (const std::string& className="JetPartonHistsAlgo");

  // these are the functions inherited from Algorithm
  virtual EL::StatusCode setupJob (EL::Job& job);
  virtual EL::StatusCode histInitialize ();
  virtual EL::StatusCode initialize ();
  virtual EL::StatusCode execute ();
  virtual EL::StatusCode histFinalize ();

private:
  int m_cf_n23s;

  // this is needed to distribute the algorithm to the workers
  ClassDef(JetPartonHistsAlgo, 1);
};

#endif
