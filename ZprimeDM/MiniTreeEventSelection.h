#ifndef ZprimeDM_MiniTreeEventSelection_H
#define ZprimeDM_MiniTreeEventSelection_H

#include <EventLoop/StatusCode.h>
#include <EventLoop/Algorithm.h>
#include <EventLoop/Worker.h>

// rootcore includes
#include <GoodRunsLists/GoodRunsListSelectionTool.h>
#include <PileupReweighting/PileupReweightingTool.h>
#include <AsgTools/AnaToolHandle.h>

//algorithm wrapper
#include "xAODAnaHelpers/Algorithm.h"

// our histogramming code
#include <ZprimeDM/DijetISREvent.h>

#include <ZprimeDM/CutflowHists.h>

class MiniTreeEventSelection : public xAH::Algorithm
{
  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.
public:
  
  //configuration variables
  bool m_mc;
  bool m_doTruthOnly;

  std::string m_triggerDetailStr;

  std::string m_jetDetailStr;
  std::string m_photonDetailStr;
  std::string m_fatjetDetailStr;
  std::string m_truthDetailStr;
  std::string m_trigJetDetailStr;

  // GRL
  bool m_applyGRL;
  std::string m_GRLxml;

  //PU Reweighting
  bool m_doPUreweighting;
  std::string m_lumiCalcFileNames;
  std::string m_PRWFileNames;
  int m_PU_default_channel;

private:

  GoodRunsListSelectionTool*                     m_grl;                //!
  asg::AnaToolHandle<CP::IPileupReweightingTool> m_pileup_tool_handle; //!

  // Cutflow
  CutflowHists *m_cutflow; //!
  int m_cf_init;
  int m_cf_grl;

  // Event data
  DijetISREvent* m_eventData; //!

  // variables that don't get filled at submission time should be
  // protected from being send from the submission node to the worker
  // node (done by the //!)
public:
  // Tree *myTree; //!
  // TH1 *myHist; //!

  // this is a standard constructor
  MiniTreeEventSelection (const std::string& className = "MiniTreeEventSelection");

  // these are the functions inherited from Algorithm
  virtual EL::StatusCode setupJob (EL::Job& job);
  virtual EL::StatusCode fileExecute ();
  virtual EL::StatusCode histInitialize ();
  virtual EL::StatusCode changeInput (bool firstFile);
  virtual EL::StatusCode initialize ();
  virtual EL::StatusCode execute ();
  virtual EL::StatusCode postExecute ();
  virtual EL::StatusCode finalize ();
  virtual EL::StatusCode histFinalize ();

  // this is needed to distribute the algorithm to the workers
  ClassDef(MiniTreeEventSelection, 1);
};

#endif
