#ifndef ZprimeDM_FatJetContainer_H
#define ZprimeDM_FatJetContainer_H

#include <TTree.h>
#include <TLorentzVector.h>

#include <vector>
#include <string>

#include <JetSubStructureUtils/BosonTag.h>

#include <xAODAnaHelpers/HelperClasses.h>
#include <xAODAnaHelpers/HelperFunctions.h>

#include <ZprimeDM/FatJet.h>
#include <ZprimeDM/ParticleHelpTree.h>
#include <ZprimeDM/JetHelpTree.h>

#include "InDetTrackSelectionTool/InDetTrackSelectionTool.h"


namespace ZprimeDM
{

  class FatJetHelpTree : public ParticleHelpTree<FatJet,HelperClasses::JetInfoSwitch>
  {
  public:
    FatJetHelpTree(const std::string& name = "fatjet", const std::string& detailStr="", float units = 1e3, bool mc = false);
    virtual ~FatJetHelpTree();
    
    virtual void createBranches(TTree *tree);
    virtual void clear();
    virtual void fillFatJet( const xAOD::Jet* jet );

    float       m_trackJetPtCut;
    float       m_trackJetEtaCut;
    std::string m_trackJetName;

  private:

    JetSubStructureUtils::BosonTag*      m_WbosonTaggerMedium;
    JetSubStructureUtils::BosonTag*      m_ZbosonTaggerMedium;
    JetSubStructureUtils::BosonTag*      m_WbosonTaggerTight ;
    JetSubStructureUtils::BosonTag*      m_ZbosonTaggerTight ;

    ZprimeDM::JetHelpTree* m_trkJets;
    bool SelectTrackJet(const xAOD::Jet* TrackJet);
  };
}



#endif // ZprimeDM_FatJetHelpTree_H
