#ifndef ZprimeDM_JetConstituent_H
#define ZprimeDM_JetConstituent_H

#include "ZprimeDM/Particle.h"

namespace ZprimeDM
{

  class JetConstituent : public Particle
  {
    ClassDef(JetConstituent, 1);

  public:      
    JetConstituent() : Particle() {};
    virtual ~JetConstituent() {};
  };

} //ZprimeDM
#endif // ZprimeDM_JetConstituent_H
