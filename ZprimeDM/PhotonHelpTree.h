#ifndef ZprimeDM_PhotonHelpTree_H
#define ZprimeDM_PhotonHelpTree_H

#include <TTree.h>
#include <TLorentzVector.h>

#include <vector>
#include <string>

#include "xAODEgamma/PhotonContainer.h"

#include <xAODAnaHelpers/HelperClasses.h>

#include <ZprimeDM/Photon.h>
#include <ZprimeDM/ParticleHelpTree.h>

namespace ZprimeDM
{

  class PhotonHelpTree : public ParticleHelpTree<Photon,HelperClasses::PhotonInfoSwitch>
  {
  public:
    PhotonHelpTree(const std::string& name = "ph", const std::string& detailStr="", float units = 1e3, bool mc = false);
    virtual ~PhotonHelpTree();
    
    virtual void createBranches(TTree *tree);
    virtual void clear();
    virtual void fillPhoton( const xAOD::Photon* photon );

  private:
    // PID
    int m_n_IsLoose;
    int m_n_IsMedium;
    int m_n_IsTight;
  };
}
#endif // ZprimeDM_PhotonHelpTree_H
