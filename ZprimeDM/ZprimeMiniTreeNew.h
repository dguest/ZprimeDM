#ifndef ZprimeDM_ZprimeMiniTreeNew_H
#define ZprimeDM_ZprimeMiniTreeNew_H

#include "ZprimeDM/HelpTreeBase.h"
#include "TTree.h"

class ZprimeMiniTreeNew : public ZprimeDM::HelpTreeBase
{

  private:

    float m_weight;
    float m_weight_corr;
    float m_weight_xs;

    float m_Zprime_pt;
    float m_Zprime_eta;
    float m_Zprime_phi;
    float m_Zprime_m;

  public:

    ZprimeMiniTreeNew(xAOD::TEvent * event, TTree* tree, TFile* file);
    ~ZprimeMiniTreeNew();

    void AddEventUser   ( const std::string& detailStr = "" );
    void FillEventUser  ( const xAOD::EventInfo* eventInfo );
    void ClearEventUser ();
};
#endif
