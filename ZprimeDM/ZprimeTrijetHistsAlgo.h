#ifndef ZprimeDM_ZprimeTrijetHistsAlgo_H
#define ZprimeDM_ZprimeTrijetHistsAlgo_H

// algorithm wrapper
#include <ZprimeDM/ZprimeHistsBaseAlgo.h>

class ZprimeTrijetHistsAlgo : public ZprimeHistsBaseAlgo
{
public:
  double m_minLeadingJetPt;          // LeadingPtCut - cut on the leading jet pt

protected:
  virtual void initISRCutflow();
  virtual bool doISRCutflow();

public:
  // this is a standard constructor
  ZprimeTrijetHistsAlgo ();

private:
  // cutflow
  int m_cf_njets;
  int m_cf_leadingjet;

  // this is needed to distribute the algorithm to the workers
  ClassDef(ZprimeTrijetHistsAlgo, 1);
};

#endif
