#ifndef ZprimeDM_ZprimeMGTrijet2DHists_H
#define ZprimeDM_ZprimeMGTrijet2DHists_H

#include <xAODAnaHelpers/HistogramManager.h>
#include <xAODAnaHelpers/HelperClasses.h>

#include <ZprimeDM/ZprimeHelperClasses.h>
#include <ZprimeDM/DijetISREvent.h>
#include <ZprimeDM/TruthHists.h>

class ZprimeMGTrijet2DHists : public HistogramManager
{
public:
  ZprimeMGTrijet2DHists(const std::string& name, const std::string& detailStr="");
  virtual ~ZprimeMGTrijet2DHists();

  virtual StatusCode initialize();
  virtual StatusCode execute(const xAH::TruthPart* jets[3], float eventWeight);
  using HistogramManager::book; // make other overloaded version of book() to show up in subclass
  using HistogramManager::execute; // overload

private:
  TH2F* h_m13vsm23;

  TH2F* h_asymjj13vsasymjj23;
};

#endif
