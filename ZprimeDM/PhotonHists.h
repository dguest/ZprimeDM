#ifndef ZprimeDM_PhotonHists_H
#define ZprimeDM_PhotonHists_H

#include <xAODAnaHelpers/HistogramManager.h>

#include <ZprimeDM/DijetISREvent.h>

namespace ZprimeDM 
{
  class PhotonHists : public HistogramManager
  {
  public:

    PhotonHists(const std::string& name, const std::string& detailStr, const std::string& prefix="");
    virtual ~PhotonHists() ;

    bool m_debug;
    virtual StatusCode initialize();

    StatusCode execute(const xAH::Photon*      photon, float eventWeight);
    StatusCode execute(const ZprimeDM::Photon* photon, float eventWeight);
    using HistogramManager::book;    // make other overloaded version of book() to show up in subclass
    using HistogramManager::execute; // make other overloaded version of execute() to show up in subclass

  private:
    HelperClasses::PhotonInfoSwitch m_infoSwitch;

    std::string m_prefix;

    //histograms

    // kinematic
    TH1F* h_pt;
    TH1F* h_pt_m;
    TH1F* h_pt_l;
    TH1F* h_eta;
    TH1F* h_phi;
    TH1F* h_m;

    // isolation
    TH1F* h_isIsolated_FixedCutTightCaloOnly;
    TH1F* h_isIsolated_FixedCutTight;
    TH1F* h_isIsolated_FixedCutLoose;
    TH1F* h_ptcone20;
    TH1F* h_ptcone30;
    TH1F* h_ptcone40;
    TH1F* h_ptvarcone20;
    TH1F* h_ptvarcone30;
    TH1F* h_ptvarcone40;
    TH1F* h_topoetcone20;
    TH1F* h_topoetcone30;
    TH1F* h_topoetcone40;

    // PID
    TH1F* h_PhotonID_Loose;
    TH1F* h_PhotonID_Medium;
    TH1F* h_PhotonID_Tight;
  };
}

#endif // ZprimeDM_PhotonHists_H
