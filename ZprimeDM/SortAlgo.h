#ifndef ZprimeDM_SortAlgo_H
#define ZprimeDM_SortAlgo_H

// algorithm wrapper
#include <ZprimeDM/ZprimeAlgorithm.h>

#include <xAODAnaHelpers/HelperFunctions.h>

#include <AsgTools/MessageCheck.h>

class SortAlgo : public ZprimeAlgorithm
{
  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.
public:

  std::string m_inContainerName;    // Input container
  std::string m_outContainerName;   // Output container

  // useulf functions
private:
  template<typename T> EL::StatusCode sort()
  {
    const T *inParticles;
    if(HelperFunctions::retrieve(inParticles, m_inContainerName, m_event, m_store)!=EL::StatusCode::SUCCESS) return EL::StatusCode::FAILURE;

    ConstDataVector<T> *outParticles=new ConstDataVector<T>(SG::VIEW_ELEMENTS);
    for(auto particle : *inParticles) outParticles->push_back(particle);
    std::sort(outParticles->begin(), outParticles->end(), HelperFunctions::pt_sort());

    ANA_CHECK(m_store->record(outParticles, m_outContainerName ));

    return EL::StatusCode::SUCCESS;
  }

  // variables that don't get filled at submission time should be
  // protected from being send from the submission node to the worker
  // node (done by the //!)
public:
  // Tree *myTree; //!
  // TH1 *myHist; //!



  // this is a standard constructor
  SortAlgo (const std::string& className="SortAlgo");

  // these are the functions inherited from Algorithm
  virtual EL::StatusCode setupJob (EL::Job& job);
  virtual EL::StatusCode execute ();

  // this is needed to distribute the algorithm to the workers
  ClassDef(SortAlgo, 1);
};

#endif
