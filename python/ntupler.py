#!/usr/bin/env python

import sys
import time
import tempfile
import getpass
import subprocess
import os
import argparse

from xAODAnaHelpers import Config, utils

commonflags={}
commonflags['bkg']='--isMC'
commonflags['sig']='--isMC'
commonflags['fastsim']='--isMC --isAFII'

def parse_types(option):
    types=[]
    for optionkey in option.keys():
        if optionkey.startswith('filelists_'):
            types.append(optionkey[10:])
    return types

def create_argparser(options=None):
    #
    # Determine some useful defaults

    # If you want the output to go to a particular disk, set it here.
    # Set default localgroupdisk to look for
    # Add your username here if you like :)
    # If you add a localGroupDisk from the command line it will
    # overwrite the one here.
    destinations={}
    username = getpass.getuser()
    destinations['kpachal']="CA-SFU-T2_LOCALGROUPDISK"
    destinations['ecorriga']="SE-SNIC-T2_LUND_LOCALGROUPDISK"
    destinations['kkrizka']="NERSC_LOCALGROUPDISK"

    #
    # Create the parser object
    parser = argparse.ArgumentParser(description='Launch the ntuple making process.')
    parser.add_argument('-p','--pretend', action='store_true', help='Print only submission commands.')

    # Add options for running
    if options!=None:
        for key,config in options.items():
            types=['all']
            for configkey in config.keys():
                if configkey.startswith('filelists_'):
                    types.append(configkey[10:])
            parser.add_argument('--{0}'.format(key), metavar=','.join(types), help='Run over listed {config} configurations.'.format(config=key))

    # Specify where to run jobs
    drivers_parser = parser.add_subparsers(prog='runntupler.py', title='drivers', dest='driver', description='specify where to run jobs')

    direct = drivers_parser.add_parser('direct',help='Run your jobs locally.')
    grid = drivers_parser.add_parser('grid',help='Run your jobs on the grid.')
    grid.add_argument('--destination', type=str, required=False, default=destinations.get(username,None))
    grid.add_argument('--tag', type=str, required=True)

    return parser

def runntupler(options):
    parser = create_argparser(options)
    args = parser.parse_args()

    ##-----------------------------------------------##
    ## Organisation and running
    user = getpass.getuser()
    tag = time.strftime("%Y%m%d")

    #
    # Prepare output directory
    if not "direct" in args.driver:
        datadir = tempfile.mkdtemp(suffix="{0}_ZPrime_DM_{1}_{2}".format(user,tag,args.driver))
    else :
        datadir = os.environ.get('DATADIR',os.getcwd())

    # Declare options for running with different drivers
    if args.driver=="direct":
        runcode ="--nevents=10000 direct"
    elif args.driver=="grid":
        dest = ""
        if args.destination!=None:
            dest += "--optGridDestSE={0} ".format(args.destination)
        runcode ="prun {dest} --optGridOutputSampleName=user.%nickname%.%in:name[1]%.%in:name[2]%.%in:name[3]%.SELECTION.{tag}/".format(dest=dest,tag=args.tag)
    else :
        print("Unrecognized mode: {0}".format(args.driver))
        return

    # Prepare commands for running
    if utils.is_release20():
        commandFormat = "xAH_run.py --config ZprimeDM/data/config_{selection}_ntuple.py --files {files} --inputList --inputRucio {flags} --submitDir {datadir}/{outname} --force {runcode} "
        filelistdir='ZprimeDM/filelists/R20p7_lists'
    else:
        commandFormat = "xAH_run.py --config ../ZprimeDM/data/config_{selection}_ntuple.py --files {files} --inputList --inputRucio {flags} --submitDir {datadir}/{outname} --force {runcode} "
        filelistdir='../ZprimeDM/filelists'
    commands = []

    enablealloptions=all([getattr(args,key)==None for key in options.keys()])

    for key,option in options.items():
        print("Beginning option {0}".format(key))

        # Skip any options diabled via argument
        enabled=getattr(args,key)
        if not enabled and not enablealloptions:
            print('\tskip...')
            continue

        # Get list of enabled types
        enabled=enabled.split(',') if not enablealloptions else ['all']
        types=parse_types(option)
        if 'all' not in enabled:
            enabled = filter(lambda x: x in enabled, types)
        else:
            enabled = types

        # Loop over types and prepare a command for each
        for type in enabled:
            # Prepare the filelist
            files=' '.join('{filelistdir}/{filelist}.{derivation}.list'.format(filelistdir=filelistdir,
                                                                               filelist=filelist,
                                                                               derivation=option['derivation'])
                           for filelist in option['filelists_'+type])
            flags=commonflags.get(type,'')
            outname='OUT_{option}_{type}_ntuple'.format(option=key,
                                                        type=type)
            runcode2=runcode.replace('SELECTION',option['selection'])

            commands.append(commandFormat.format(selection=option['selection'],
                                                 files=files,
                                                 flags=flags,
                                                 datadir=datadir,
                                                 outname=outname,
                                                 runcode=runcode2))
    #
    # Run!
    for command in commands:
        print(command+"\n")
        if not args.pretend: subprocess.call(command,shell=True)
        
    #
    # Clean-up
    if args.driver!='direct': subprocess.call("rm -rf {0}".format(datadir),shell=True)
